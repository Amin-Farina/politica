+++
author = "Stella Maris Folguerá"
author2 = ""
date = 2021-04-09T14:00:00Z
description = ""
image = "/images/placa-g6-stella-maris.jpg"
image_webp = "/images/placa-g6-stella-maris.jpg"
title = "Entrevista con mujeres radicales"

+++
Stella Maris Folguerá es artista plástica, escritora, fue la primer candidata mujer a Intendente en el año 1985 cuando no había cupo, quedando como concejal hasta el 1989 y fue presidenta de la Convención Provincial del partido entre los años 1990-1992.

Hablamos sobre su vida cultural y política; el rol de las mujeres en 1945 y cómo avanzamos hacia la actualidad; sus obras literarias y especial interés hacia Eulalia Ares, la cartilla gobernadora de Argentina; la historia del Radicalismo y un mensaje de lucha a los jóvenes.

"Tengan en la mesita de luz la profesión de fe doctrinaria y el mensaje de Parque norte y repásenlo siempre. Tengan present que de los radicales la sociedad siempre espera más, nos exigen más, porque hemos sido el partido del discurso de la ética, de la moral y de las garantías constitucionales y no podemos apartarnos de eso. No podemos dejar que ninguno que se diga radical se aparte de eso, por mucho poder que distensión ".

Para ver el video completo, imperdible, [ingresa aquí.](https://youtu.be/XTu7vQdsWvw "Entrevista completa")