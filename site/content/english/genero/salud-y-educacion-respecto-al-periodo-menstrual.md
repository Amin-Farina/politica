+++
author = "La 30 de octubre Corrientes"
author2 = ""
date = 2021-03-19T14:00:00Z
description = ""
image = "/images/placa-g4-30de-oct.jpg"
image_webp = "/images/placa-g4-30de-oct.jpg"
title = "Salud y Educación respecto al período menstrual"

+++
"Debemos enfocar los esfuerzos en la salud menstrual y no sólo en el manejo de la higiene menstrual. La gestión de la salud menstrual incluye factores sistémicos y socioculturales esenciales para el desarrollo. Creemos firmemente que para garantizar una atención integral es nuestra responsabilidad empatizar con la otra persona."

Para ver el video completo de las jóvenes militantes de La 30 de Octubre, ingresa aquí --> [https://www.youtube.com/watch?v=k8fCx5TXcZo](https://www.youtube.com/watch?v=k8fCx5TXcZo "Gestión de la salud menstrual ")