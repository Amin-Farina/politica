+++
author = "Juventud Radical Provincia de Corrientes"
author2 = ""
date = 2022-08-31T11:00:00Z
description = ""
image = "/images/placa-g7-paridad-2.jpg"
image_webp = "/images/placa-g7-paridad-2.jpg"
title = "Ley de Paridad de Genero en Corrientes"

+++
**_¿Qué es la Paridad de género?_**

Paridad es una medida afirmativa que se incorpora con la finalidad de corregir una desigualdad de base.

**_¿Por qué es importante?_**

Porque la Ley de paridad busca incorporar la participación política equitativa entre géneros en todos los cargos electivos legislativos establecidos por la Constitución Nacional y Provincial.

Porque la igualdad política se refiere a la igualdad de representación y a la participación en la toma de decisiones que afectan a la realidad de cada una de las correntinas.

La Paridad no es un techo o limite, sino una vía para asegurar un piso justo de participación.

**_¿Por qué se aplica como una medida afirmativa?_**

Porque si bien todas y todos somos iguales ante la ley, la realidad es distinta.

Las distorsiones de un sistema patriarcal generan discriminaciones estructurales.

La lucha por la igualdad es nuestro motor que nos impulsa hacia adelante.

Por eso como jóvenes militantes, celebramos la sanción de la ley de paridad de género.

Juntas seguiremos trabajando con el compromiso de siempre para ocupar los espacios que esta Ley nos reconoce y luchando por mas y real igualdad.

_#NUNCAMASSINNOSOTRAS_

_#NOSOMOSTANPOCASNIESTAMOSTANSOLAS_

_#LOS DERECHOS SON RESULTADOS DE LAS LUCHAS DE LAS MUJERES_

Para ver el video completo accedé acá: [https://www.youtube.com/watch?v=iU2fs7uaTu0](https://www.youtube.com/watch?v=iU2fs7uaTu0 "Paridad de Género en Corrientes")

**Militantes radicales del Comité Provincial de Corrientes.-**