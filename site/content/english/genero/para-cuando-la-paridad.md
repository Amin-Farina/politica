+++
author = "Juliana Frete"
author2 = ""
date = 2020-10-28T03:00:00Z
description = ""
image = "/images/placa-g2-juli.jpg"
image_webp = "/images/placa-g2-juli.jpg"
title = "¿Para cuando la Paridad?"

+++
Podría enumerar una serie de argumentos legislativos que se encuentran plasmados en nuestra Constitución Nacional y en numerosos Convenios Internacionales de Protección y Promoción de Derechos Humanos, que además, no solo sugieren que el Estado argentino aplique acciones positivas tendientes a lograr una equidad de género, sino que lo exigen.

Sin embargo, prefiero interpelarnos sobre la importancia de la búsqueda de una **representatividad de, por y para las mujeres** deteniéndonos en algunos puntos ​de este largo camino que a las argentinas nos ha llevado tantos años.

Siempre que se plantea esta cuestión, nos enfrentamos a tener que esforzarnos muchísimo para explicar por qué no es un privilegio; a pesar de que está a la vista de todos y todas.

“Tienen cupo, ¿quieren más?” Claro, no basta con ser relegadas a un tercio de una lista cuando una equidad real se puede concretar de otra manera, por ejemplo intercalando un hombre y una mujer.

“¿Es una cuestión de número?” Sí, lo es. El número nos permite tomar decisiones; las que nos involucran pero que siempre miramos desde afuera a la espera de la opinión de los hombres; las que van mucho más allá de una condición simplista referida al sexo, sino que comprenden a la mujer como actor colectivo.

Debemos defendernos de quienes nos dicen que de este modo solo accederán las que pertenezcan a un determinado tipo de mujer, aquellas hijas, esposas y hasta algunos no tienen vergüenza de decir amantes “de”; atreviéndose de este modo a asegurar que nuestro “capricho”, de hecho, solamente colmará los espacios de mujeres inexpertas y poco o casi nada calificadas para ocuparlos. Me pregunto entonces, ¿quiénes miden la capacidad de los hombres? ¿y en base a qué criterios? Mejor, hagamos el ejercicio de calcular cuántas de las referentes barriales que conocemos han podido llegar a verdaderos espacios de poder. Quizás suene hasta agresivo, pero mientras algunos lloran meritocracia, al mismo tiempo, vemos candidatos no solo de dudosa capacidad sino hasta denunciados por violencia de género.

“HAY MUJERES EN POLÍTICA, PERO FALTAN POLÍTICAS PARA LAS MUJERES EN ARGENTINA” manifiesta insistentemente la Diputada Nacional Carla Carrizo, ¿cómo no compartir firmemente esta expresión? ¿No es completamente cierta? El Estado debe garantizar políticas para que cada vez existan menos desigualdades de género en nuestro país y estoy convencida de que ello sólo se podrá obtener con equidad y perspectiva de género.

Tenemos muchísimos desafíos, es real que con una paridad en todos los poderes del Estado no la aseguramos enteramente, pero lo cierto es que, debemos al menos, equiparar la representatividad.

Sabemos perfectamente que exigir paridad es disputar poder, pero no vamos a retroceder ni por un segundo en la lucha por el reconocimiento de nuestros derechos a pesar de la incomodidad que esto pueda generar en algunos ámbitos. En especial, en el político.

En estas circunstancias es que tenemos presente que la única forma de conseguirla es mediante la sociedad en su conjunto; no alcanza con aspirar verla en el Estado cuando las niñas, adolescentes y mujeres pasan su vida entera sorteando obstáculos designados para ellas únicamente. Eso nos permite no confundirnos, la paridad no tiene su razón en un grupo de mujeres del Congreso Nacional que pretendía ocupar alguna banca más; la paridad tiene su razón de ser en la interpretación de la demanda social que reclama igualdad de condiciones en cada espacio en el que intentemos desarrollarnos.

Nos afecta de una manera transversal y colectiva que por supuesto no distingue de partidos políticos, aunque, considero que el radicalismo no puede mantenerse al margen de una discusión que tiene como objetivo mejorar la calidad de nuestra democracia, por el contrario, debemos hacer todo lo posible para instituirla; especialmente porque la Unión Cívica Radical ha cumplido papeles fundamentales en cada conquista de derechos en Argentina.

En una provincia donde está naturalizada la ausencia de mujeres en el poder y ni siquiera se cumple la ley de cupo en la Cámara de Senadores, me pregunto: ¿qué estamos haciendo los y las dirigentes y militantes por cambiarla?

En un país donde muere una mujer cada 32 horas, ¿qué estamos haciendo los y las dirigentes y militantes? ¿En serio pensamos que la paridad de género no modifica nada?

**Que la realidad nos atropelle, nos duela, o que al menos nos incomode lo suficiente todos los días para que no podamos concebir no transformarla.**

                             **_Frete Juliana - Mujer, feminista, radical y reformista._**