+++
author = "Fundación Avón"
author2 = ""
date = 2021-11-25T11:00:00Z
description = ""
image = "/images/whatsapp-image-2021-11-25-at-12-27-45.jpeg"
image_webp = "/images/whatsapp-image-2021-11-25-at-12-27-45.jpeg"
title = "No a la violencia de género"

+++
_La campaña **#SiNoHaySíEsNo** impulsada por Avon y Fundación Avon._

_Fue desarrollada por 7 ilustradorxs de Latinoamérica que acercaron mensajes sobre consentimiento de forma creativa y accesible a las personas._

Los 16 días de activismo están acá y, para este año, desde Fundación Avon decidieron aprovecharlo para hablar sobre el consentimiento. Sí, un tema que parece ser tan simple y que solo depende de dos palabras: sí o no. Pero lamentablemente, este concepto no parece ser tan fácil para algunas personas. Y es por eso que necesitamos dejarlo en claro de una forma en la que todos y todas entiendan. Entonces pensamos: tal vez necesitemos dibujarlo.

Por eso invitaron a 7 ilustradores e ilustradoras de América Latina a explicar conceptos básicos del consentimiento digital de la manera en que solo el arte puede hacerlo: con un lenguaje visual y concreto para abordar aquellos temas sensibles en los que necesitamos educarnos. Sin grises y sin lugar para las dudas. Lxs artistas que acompañaron esta campaña son: Tute, Pepita Sandwich, Helô D’Angelo, Thaliaeme, Javiera Camposano, Sara Tomate y The Karynne.

8 de cada 10 mujeres recibieron imágenes, mensajes, emojis o memes de tenor sexual sin su consentimiento; mientras que 7 de cada 10 fueron presionadas a enviar fotos íntimas incluso luego de decir que no querían, según datos relevados por la Encuesta “Consentimiento y Violencia en el Mundo Digital” realizada por Fundación Avon. ¿Cómo movilizar conciencias para la construcción de un cambio? En un contexto de mucha información, donde múltiples mensajes circulan y conversan y la confianza se vuelve un bien de alto valor, ¿de qué manera impactar para provocar reflexiones y discusiones indispensables sobre violencias de género y consentimiento?

La respuesta creativa para el desarrollo de la campaña fue la expresión artística, un puente de conexión entre los deseos, las emociones y la acción. La expresión artística que tiene una larga historia como potencia transformadora cultural. Porque el arte genera preguntas, cuestionamientos e inspira nuevas formas de pensar y hacer.

Así, la campaña **#SiNoHaySíesNo** vehiculiza un propósito de cambio a través de un canal diferente, atractivo, que se disfruta y vuelve al mensaje relevante e innovador que atraviesa la racionalidad llegando a nuestra esencia.

Para ello también fue importante interpretar que la cultura es diversa y plural y que era importante, en una campaña regional, abordar estéticas y lenguajes también diversos y plurales. Por esto en esta campaña se convocaron múltiples estilos de ilustración que a su vez personifican la variedad cultural que caracteriza a Latinoamérica.

En el camino para llegar de manera efectiva y relevante, también fue fundamental pensar en cuál era el vehículo cultural más apropiado para este mensaje. Y para eso fue necesario encontrar la clave para comunicar el comportamiento a transformar. ¿Qué tipo de expresión artística resultaba el más adecuado? La historieta, con su rol de entretenimiento y educación funcionó también como provocación: si algo resulta difícil de ver y entender, ¿querrías que te lo dibuje?

Después de todo es hora de aprender que cuando no hay un sí, es no. Hacé click [**acá**](https://bit.ly/manualdeconsentimientoilustrado) para leer el Manual de Consentimiento Ilustrado.

Fundación Avón.