+++
author = "Carolina Caamaño"
author2 = ""
date = 2020-10-02T21:00:00Z
description = "Este año se cumplieron 5 años de aquella movilización, donde la Argentina dijo basta, basta de muertes, basta de femicidios. El 3 de junio de 2015 se realizó una convocatoria Frente al Congreso de la Nación para exigir se cumpla la Ley 26.485"
image = "/images/placa-g.jpg"
image_webp = "/images/placa-g.jpg"
title = "En esta lucha no estamos todas"

+++
> “Porque al fin y al cabo el miedo de la mujer a la violencia del hombre es el espejo del miedo del hombre a la mujer sin miedo”.
>
> Eduardo Galeano[\[1\]](#_ftn1).

Este año se cumplieron 5 años de aquella movilización, donde la Argentina dijo basta, basta de muertes, basta de femicidios. El 3 de junio de 2015 se realizó una convocatoria Frente al Congreso de la Nación para exigir se cumpla la Ley 26.485 de protección integral para prevenir, sancionar y erradicar la violencia contra las mujeres.

La convocatoria se realizó principalmente a través de las redes sociales, iniciado por un grupo de periodistas y militantes a raíz del asesinato de Chiara Perez[\[2\]](#_ftn2), la misma según los registros se extendió a cerca de cien plazas en todo el país. Congregó a hombres, mujeres, adolescentes, niños y niñas, donde deportistas, artistas, y organizaciones sociales dijeron presente.

La convocatoria #NiunaMenos, se refiere a ni una mujer menos víctima de femicidio, que se convirtió en un hito que marcó un antes y un después en la lucha contra la violencia machista, y no solo en Argentina, sino que el reclamo se fue extendiendo en otras partes del mundo. Muchas ciudades y muchos países se sumaron.

Las mujeres se expresaron en redes sociales contando experiencias, repudiando los femicidios y la violencia contra la mujer. A esta lucha se sumaron el #metoo y campañas como “mira como nos ponemos”, cuando se hizo público el caso de Thelma Fardin[\[3\]](#_ftn3) a través del colectivo Actrices Argentinas, y la lucha a favor del aborto legal.

Las mujeres se convocaron, marcharon, hablaron, gritaron, por primera vez todas juntas movilizadas contra el patriarcado. Madres, hermanas, abuelas, hijas comenzaron a hablar de sororidad, este término que sin dudas inspira al movimiento feminista y que se trata de hermanamiento, solidaridad, y alianza entre las mujeres.

Este importante movimiento logró visibilizar la problemática e imponer el tema en la agenda pública, ya nadie desde ese día pudo mirar al costado, porque todos y todas sabíamos de que estábamos hablando. Sin embargo, todavía falta mucho por hacer.

En este camino los medios de comunicación tienen un papel primordial a la hora de abordar la problemática y de contar los casos, de concientizar y de brindar todas las herramientas disponibles para abordar las problemáticas.

Es necesario hablar de la responsabilidad estatal, de hacer mención a la legislación existente en nuestro país sobre la temática, y hablar de la legislación que falta, de informar lugares donde acudir, y las diferentes formas de comunicación con los centros o instituciones que brindan ayuda y contención tanto locales como nacionales.

De acompañar y fortalecer y difundir campañas de bien público, para que las mujeres sepan que no están solas, que hay instituciones preparadas para acompañarlas y ayudarlas. Las marchas, campañas, los mensajes en redes sociales, los mensajes por WhatsApp, los bancos rojos, todo ayuda a visibilizar la problemática.

En Argentina muere una mujer cada 34 horas por violencia de genero según el último informe dado a conocer en el mes de septiembre por el Observatorio Mumalá[\[4\]](#_ftn4), durante la pandemia se contabilizaron 127 femicidios en Argentina desde el 20 de marzo al 20 de septiembre.

Estas estadísticas arrojan números que duelen, calan en lo más profundo, sobre todo cuando el Estado hace poco, o no hace nada para revertir esta situación. Si bien hay políticas que se aplicaron durante estos años en contra de la violencia de la mujer es poco, o no alcanza, el presupuesto que se aplica para abordar esta problemática es paupérrimo.

Porque el 65% de los femicidios son cometidos por la pareja o ex pareja de la víctima, y el 71% de los femicidios fueron en la vivienda de la víctima, o en la vivienda compartida con el victimario, y un 19% de esas mujeres víctimas de estos femicidios había denunciado a su agresor. Sin embargo, las muertes siguen ocurriendo y la pandemia no logro disminuirlas, al contrario, el problema se vio agudizado.

Las mujeres muertas no son simplemente parte de una estadística, son familias enteras destruidas, niños y niñas que se quedan sin hijas, hermanas, sobrinas. Por eso es importante abordar esta problemática desde una perspectiva de género, en las casas, en las escuelas en los clubes en las charlas cotidianas,

Es importante que trabajemos todos juntos para erradicar la violencia de género y poner en agenda esta problemática que nos afecta, ya que la podríamos considerar una epidemia, y no hay barbijo que nos proteja. Pero si hay leyes, que tenemos que exigir que se cumplan, la Ley Micaela[\[5\]](#_ftn5), la Ley Brisa[\[6\]](#_ftn6) son algunas de las nuevas legislaciones que tienen que ser difundidas, aprendidas y estudiadas para que cada día que pase este país pueda ser un poco más justo.

Este sin dudas es el siglo de las mujeres, el de las niñas, de las adolescentes y de todas y cada una de las mujeres que lucha por su libertad. Que luchan por vivir en un mundo sin prejuicios, donde ser hombre, o ser mujer no haga la diferencia.

Desde hace muchos años las mujeres, luchamos por nuestros derechos, por el derecho a elegir libremente, a decidir por nuestros cuerpos, y por el derecho simplemente de elegir que queremos ser, sin estereotipos, sin mandatos patriarcales, sin imposiciones, simplemente ser libres.

Estas luchas por la igualdad, por ser escuchadas, por tener voz se llevaron muchas vidas a lo largo de estos siglos, muchas vidas que duelen, y recién ahora en este, el siglo XXI podemos decir que somos escuchadas, y que nuestra voz no va a ser silenciada, porque siempre habrá una mujer para hablar por las que ya no tienen voz

Es necesario seguir trabajando por la inclusión de la mujer, por su participación activa en los lugares de tomas de decisiones, donde hay todavía falta hacer mucho, es necesario seguir trabajando en leyes que promuevan la igualdad, en buscar la paridad en todos los poderes del estado.

Es urgente trabajar en programas que promuevan la Educación Sexual Integral, en todos los niveles educativos. De que los niños, niñas y adolescentes tengan información desde los primeros años. Para evitar embarazos no deseados, para evitar violaciones y abusos en los ámbitos familiares, para evitar que las niñas sean madres.

También es urgente volver a debatir la legislación sobre el Derecho al Aborto Legal, Seguro y Gratuito en el Congreso de la Nación, para que las mujeres no mueran en condiciones inhumanas. Es necesario tener educación sexual para decidir, anticonceptivos para no abortar, y aborto legal para no morir.

Estas son simplemente unas primeras líneas para reflexionar sobre la oportunidad que tenemos las nuevas generaciones para hacer las cosas distintas, todos los días los niños y las niñas nos enseñan que un mundo más igual es posible.

**Carolina Caamaño**

Periodista, feminista, militante de la Unión Cívica Radical

***

[\[1\]](#_ftnref1) Eduardo Galeano, escritor uruguayo, extracto: “Hay criminales que proclaman tan campantes ‘la maté porque era mía’, así no más, como si fuera cosa de sentido común y justo de toda justicia y derecho de propiedad privada, que hace al hombre dueño de la mujer. Pero ninguno, ninguno, ni el más macho de los supermachos tiene la valentía de confesar ‘la maté por miedo’, porque al fin y al cabo el miedo de la mujer a la violencia del hombre es el espejo del miedo del hombre a la mujer sin miedo”.

[\[2\]](#_ftnref2) Chiara Pérez, víctima de la violencia de género. La adolescente de 14 años que había desaparecido en la localidad santafesina de Rufino fue hallada asesinada y enterrada en el patio de la casa de su novio de 16 años.

[\[3\]](#_ftnref3) El colectivo de Actrices Argentinas en conferencia de prensa hizo pública la denuncia de Thelma Fardín, quien fuera abusada sexualmente por el actor Juan Darthés en Nicaragua, durante una gira del programa Patito Feo cuando ella tenía 16 años y él, 45.

[\[4\]](#_ftnref4) Observatorio Mumalá. Mujeres, Disidencias, Derechos, septiembre 2020.

[\[5\]](#_ftnref5) Establece la capacitación obligatoria en género y violencia de género para todas las personas que se desempeñan en la función pública, en los tres poderes del Estado. Se llama así en conmemoración de Micaela García, una joven entrerriana de 21 años, que fue víctima de femicidio en manos de Sebastián Wagner.

[\[6\]](#_ftnref6) La Ley brisa establece la reparación económica para los hijos de progenitores víctimas de violencia familiar o de género. Reconoce el derecho a cobrar una suma mensual y a tener cobertura de salud.