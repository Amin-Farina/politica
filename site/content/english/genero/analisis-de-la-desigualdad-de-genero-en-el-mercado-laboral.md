+++
author = "Diana Cabral"
author2 = "Pilar Fernandez y Francisco Bosco"
date = 2021-03-31T11:00:00Z
description = ""
image = "/images/placa-g5-bosco-diana-y-pilar-f.jpg"
image_webp = "/images/placa-g5-bosco-diana-y-pilar-f.jpg"
title = "Análisis de la desigualdad de Género en el mercado laboral"

+++
Con motivo del 110° Día Internacional de la Mujer y del Mes de la Mujer, es importante analizar cuáles son los espacios que tienen las mujeres dentro del mercado laboral y, como la brecha de género se sigue evidenciando al momento de medir las reales oportunidades que tiene una mujer dentro del mundo del trabajo.

En los últimos años se fue avanzando e incluyendo con mayor fuerza dentro de la agenda de estadísticas que se generan dentro de los gobiernos, diferentes indicadores como tipos de trabajo, condiciones y legislaciones entorno a estudiar cómo la mujer se inserta dentro del mundo laboral. Las estadísticas una vez que se analizan y contextualizan, determinan ciertas formas de comportamiento de la sociedad e impacta directamente en la toma de decisiones.

Frente a los múltiples factores de género que dificultan que las mujeres puedan acceder, y permanecer en ámbitos de trabajo registrado, el objetivo principal es la transformación del espacio público mediante políticas dirigidas a diluir las estructuras que sostienen la desigualdad para asegurar el acceso de las mujeres al mundo del empleo calificado.

PARTICIPACIÓN LABORAL DE LAS MUJERES

En la última parte del siglo XX, en los países occidentales el trabajo remunerado era principalmente un ámbito masculino. Era el hombre el que necesitaba ganar el sustento de toda la familia. En las últimas décadas esta situación ha cambiado radicalmente y cada vez hay más mujeres que entran en el ámbito laboral, dando lugar a lo se ha dado en llamar la feminización del trabajo (Caraway, 2007).

De acuerdo al dossier publicado por el INDEC el 8 de marzo: “La participación en el mercado laboral favorece la autonomía económica de las mujeres. Más allá de los importantes avances - en octubre de 1996 la tasa de empleo de las mujeres a nivel país, era de 32,8%- , todavía hoy las mujeres participan menos en el mercado laboral que los varones”.

Actualmente a nivel país, la tasa de empleo en hombres es de 57,7% frente a un 39,4% en mujeres. La brecha de género continúa siendo alta al momento de analizar estos indicadores.

De acuerdo al CEPAL, las brechas de género corresponden a la medida que muestra la distancia entre mujeres y hombres respecto a un mismo indicador. Es decir, refleja la brecha existente entre los sexos respecto a las oportunidades de acceso y control de recursos económicos, sociales, culturales y políticos, entre otros.

![](/images/brecha-1.png)

CAMBIOS DE PARADIGMA

El perfil de la mujer fue transformándose durante los años y esto se ve reflejado en los números. Partiendo de que la esperanza de vida en mujeres es mayor que la de los hombres, ya que de acuerdo al INDEC las mujeres en promedio viven 6 años más que los hombres, también se agrega un cambio de paradigma que se produjo en el ámbito de la decisión de las mujeres de convertirse en madres.

De acuerdo a las cifras sobre este tema publicadas por la Dirección de Estadísticas y Censos de la Provincia, hay un descenso sostenido en los niveles de fecundidad reflejando que las mujeres tienen cada vez menos hijos. El promedio en 1960 eran 5 hijos, contra el 2019 en el que la cifra se reduce a 2 hijos. Esto nos indica que las mujeres tienen en promedio 3 hijos menos.

![](/images/brecha-2.png)

Hoy en día, sumado a estas características, las estadísticas nos muestran que las mujeres han alcanzado niveles educativos más altos que los hombres. En Corrientes, las mujeres alcanzaron niveles de educación de grado más altos que los hombres, siendo un 27,3% las mujeres que poseen el grado superior universitario completo frente a un 17,64% por parte de los hombres. (Fuente: Encuesta de Calidad de Vida - Aglomerado Corrientes Capital año 2020)

![](/images/brecha-3.png)

Aunque en promedio tienen un nivel de educación más alto que los hombres, las mujeres siguen mostrando una menor participación en el mercado laboral y, cuando lo tienen, son más propensas al subempleo y al desempleo.

Según datos proporcionados por la Dirección de Estadística y Censos de la Provincia, solamente el 5% de la direcciones de empresas se encuentran ocupadas por mujeres. Esto se debe a que persiste una división sexual en el ámbito laboral que restringe las oportunidades de desarrollo para las mujeres.

![](/images/brecha-4.png) ![](/images/brecha-5.png) ![](/images/brecha-6.png)

_Gráfico 2: Población de 14 años y más. Tasas de Actividad, de Empleo y Desocupacion por sexo. Aglomerado urbano Corrientes. Año 2019. Fuente: INDEC EPH_

La tasa de Actividad en los hombres se eleva 18 puntos porcentuales por encima del de las mujeres en el 2019, y la misma situación se repite con el Empleo. La Desocupación es mayor dentro del grupo femenino, llegando en este caso a un punto porcentual más que la tasa masculina. En definitiva los hombres tienen los indicadores a favor, dejando a los indicadores de las mujeres en desventaja. Esto también se puede ver cuando se analiza la desocupación por grupos etarios en la Tabla 1.

![](/images/brecha-7.png)

_Tabla 1_

La principal causa está en las actividades que realizan las mujeres en el espacio privado, ya que destinan mayor tiempo a las actividades domésticas y de cuidados, en comparación con los hombres.

Según datos obtenidos en la Encuesta de Calidad de Vida que realiza la Dirección de Estadística y Censos de la Provincia, del universo que refiere a las tareas del hogar, un 82,5% son realizadas por mujeres frente a un 17,5% correspondiente a los hombres.

Inclusive si miramos en profundidad cuáles son las ramas laborales en las que sobresalen las mujeres coinciden con las tareas de cuidado y domésticas que realizan en sus hogares.

El gráfico a continuación realizado con datos obtenidos de la Encuesta Permanente de Hogares pone en evidencia cómo la penetración de la mujer en ambientes laborales que no sean el cuidado de la salud (52,9%), la enseñanza (77,60%) o el servicio doméstico (92,10%) presenta barreras constantes de inserción.

![](/images/brecha-8.png)

EMPLEO NO REGISTRADO

Según el INDEC, las mujeres se insertan principalmente en sectores vinculados al cuidado como el de los hogares y la salud. Con una importante presencia de empleo informal e ingresos laborales más bajos, el servicio doméstico es la rama de ocupación con mayor índice de feminización en la Argentina. En el sector de la salud y los servicios sociales, 7 de cada 10 personas ocupadas son mujeres.

Podemos observar como el Gráfico número 3 presenta la situación de informalidad respecto a mujeres y varones. Allí la brecha decrece desde el cuarto trimestre del 2018, es decir, se ha reducido la desigualdad en cuestiones relacionadas a la contratación de trabajo informal.

![](/images/brecha-9.png)

Gráfico 3 : Brecha de empleo no registrado

En base a esto, se analiza a continuación la diferencia de sexo en cuanto al salario, teniendo en cuenta por un lado la diferencia dentro de los empleados formales y por otro la diferencia dentro de los informales.

Esto impacta en la imposibilidad que enfrentan las mujeres de alcanzar su autonomía económica, generar ingresos y recursos propios a partir del acceso al trabajo remunerado en igualdad de condiciones que los hombres. (CEPAL)

Es importante destacar en este punto que la situación del COVID, la cual impacto a nivel mundial, acentuó los problemas de participación en la actividad económica tanto de mujeres como de hombres.

Es así que observamos en el siguiente gráfico como la tasa de actividad en el segundo trimestre 2020 en Corrientes Capital es la más baja de los últimos 2 años, y se debe directamente al efecto que tuvo el Aislamiento Social Preventivo y Obligatorio en la sociedad y especialmente en la economía.

![](/images/brecha-10.png)

_Gráfico 1 - Fuente Encuesta Permanente de Hogares INDEC - Elaborado por la Dirección de Estadística y Censos de la Provincia de Corrientes_

Cuando observamos los indicadores salariales, en el 2019, los hombres cobraron 20,1% más, en promedio, de lo que cobraron las mujeres. Sin embargo, esta brecha disminuye si se compara a los hombres y las mujeres que contaban con un empleo formal, en este caso la brecha es del 12,1%, 8 puntos porcentuales menos.

![](/images/brecha-11.png)

Estos indicadores de disminución en la brecha se deben a que se está analizando el grupo de trabajadores que tiene un empleo formal, el cual está protegido por las instituciones que regulan el mercado laboral, por lo que las desigualdades salariales de acuerdo al género del trabajador deberían ser menores.

El principal problema de desigualdad de género se encuentra en los trabajadores no registrados en los sistemas de seguridad social. En este caso, los hombres en condición de informalidad ganan un 36.9% más que las mujeres.

¿CUÁL ES EL CAMINO HACIA UNA TRANSFORMACIÓN REAL?

Frente a un escenario tan desigual en cuestión de género, el primer paso para cambiarlo es visibilizar cuál es la situación en base a los indicadores donde se encuentran las desigualdades y brechas más profundas.

A través de su visibilización es que se pueden tomar medidas e implementar acciones para comenzar a transitar un camino de transformación del lugar que tiene la mujer dentro del mundo laboral.

Comenzando como lo explica claramente el CEPAL en sus numerosos estudios sobre el tema, reconociendo el valor productivo del trabajo doméstico, de cuidado no remunerado y aquellas ramas de trabajo donde prima la participación de mujeres por sobre hombres.

Para así incorporarlo a un sistema de protección social donde se pueda maximizar la autonomía económica de las mujeres, impulsar políticas públicas e implementar cambios culturales en el que la premisa sea que las tareas de cuidado pueden ser compartidas entre mujeres y hombres.

Este camino representa un desafío que no solamente atañe a los gobiernos sino que es transversal a instituciones, al sector privado, a las organizaciones sin fines de lucro, y a la comunidad en general con las mujeres y hombres que la integran.

Es un desafío de toda la sociedad poder lograr que de a poco se vayan eliminando las desigualdades y las asimetrías de género. Esto contribuiría a una sociedad más igualitaria en términos laborales para poder romper los techos y las paredes de cristal que actualmente limitan a que las mujeres accedan a trabajos de calidad y una vez en ellos, poder asegurar su permanencia en los mismos.

Dossier de Estadística y Censos de la Provincia de Corrientes

[https://estadistica.corrientes.gob.ar/download_publicacion/59](https://estadistica.corrientes.gob.ar/download_publicacion/59 "https://estadistica.corrientes.gob.ar/download_publicacion/59")

Dossier Estadístico del INDEC

[https://www.indec.gob.ar/ftp/cuadros/publicaciones/dosier_estadistico_8M_2021.pdf](https://www.indec.gob.ar/ftp/cuadros/publicaciones/dosier_estadistico_8M_2021.pdf "https://www.indec.gob.ar/ftp/cuadros/publicaciones/dosier_estadistico_8M_2021.pdf")

**_Pilar Fernández. Periodista_**

**_Dra. Diana Cabral_**

**_Lic. Francisco Bosco_**