+++
author = "Jonathan Paredes"
author2 = ""
date = 2020-11-24T13:00:00Z
description = ""
image = "/images/placa-e3.jpg"
image_webp = "/images/placa-e3.jpg"
title = "Concientización y cuidado ambiental"

+++
### **¿Qué podemos hacer al respecto desde nuestras comunidades?**

2020

_Caá Cati – Entrevista de Jonathan Alexander Paredes a la Lic. en Botánica, Gloria Ninfa Martínez, actualmente miembro del Concejo Deliberante de la ciudad de Caá Catí._

Las cuestiones vinculadas al cuidado del ambiente es una temática que ha ido tomando gran relevancia en los últimos años y es un tema que nos atañe a todo y del cual deberíamos como actores principales de su degradación, tomar conciencia de nuestro accionar sobre el ambiente, las consecuencias que ello acarrea y que rol podemos llevar a cabo para evitar consecuencias negativas de nuestro accionar.

Una cuestión negativa en cuanto a este asunto es que un gran porcentaje de personas desconoce o tal vez ni imagina el gran perjuicio que se ocasiona diariamente sobre el ambiente con los desechos que producimos, la contaminación del aire debido a los gases despedidos por vehículos e industrias, la contaminación de ríos provocada por las aguas servidas, las quemas indebidas de pastizales que luego se convierten en incendios forestales, la tala de árboles, entre otros. Por ello es necesario que conozcamos y tomemos conciencia de las problemáticas que son consecuencias nuestro mal accionar y a partir de ello aportar ideas y trabajar en proyectos que posibiliten minimizar o evitar a futuro los impactos que podríamos generar sobre el ambiente.

Actualmente la emergencia climática y ambiental parece recordarnos que existe una relación de importancia entre la salud y el ambiente y es necesario tomar conciencia de tal relación para lograr la iniciativa de llevar a cabo acciones que nos permitan contribuir al cuidado del ambiente.

Es por ello que en esta ocasión decidí realizar una visita a la Lic. en Botánica, Gloria Ninfa Martínez, quien actualmente es miembro del consejo deliberante de la ciudad de Caá Catí para que nos comente su proyecto de arborización que está llevando a la práctica en la comunidad.

· **_¿En qué consiste el proyecto de arborización?_**

_El proyecto de arborización surge a partir de los hechos ocurrido en el amazonas, las miles y miles de hectáreas que se han incendiado y por lo tanto la cantidad de especies vegetales que se perdieron, sabiendo que la vegetación es fundamental para el cuidado del ambiente y la purificación de la masa gaseosa que es imprescindible para los seres vivos._

_Entonces, surgió la idea de realizar un proyecto de arborización teniendo en cuenta que Caá Catí tiene lugares carentes de árboles y que se encuentra en una zona subtropical._

_Surge este proyecto diciendo: tal vez aquí en nuestra localidad, con este pequeño acto, estamos contribuyendo a palear en parte esta problemática mundial que surge a partir de la desforestación y de los incendios. Pero, sin embargo con la fe de que localidades cercanas u otras entidades, organizaciones y grupos, imiten este gesto y todos nos unamos tratando de revertir o por lo menos mantener la situación ambiental._

· **_¿Qué la motivó a proponer dicho proyecto?_**

_En la pregunta anterior respondí en parte el motivo que me llevó a establecer este proyecto pero agregaría que además de concejal soy docente del nivel secundario y la idea es sumar esfuerzos con mis alumnos de sexto año en las asignaturas que llevan conmigo que es ecología- ambiente y sociedad para que mis alumnos se vayan concientizando de la importancia de ir a los hechos, teniendo el deseo de que ellos también sean protagonistas de ese cambio que queremos lograr como sociedad en favor del ambiente._

· **_¿Qué repercusiones obtuvo como respuesta respecto al proyecto?_**

_En realidad tengo que estar muy agradecida a la población en general porque realmente tuvo repercusiones. Al ser sincera tenía miedo de que el proyecto quede, como se dice comúnmente, “encajonado” porque muchas veces en la política ocurren esas cuestiones. Pero en realidad estoy muy contenta en primer lugar por la repercusión dentro del área de la Dirección de Cultura y Ambiente del municipio, ya que el responsable que es el joven Matias Geneyro, director del área, se acercó e inmediatamente se puso en contacto conmigo, así como también otras personas como ser ingenieros agrónomos, representantes del INTA y directivos de escuelas, para poner de inmediato en práctica el proyecto._

_En segundo lugar, a todas las personas a las cuales se los ha convocado y que actuaron positivamente, por ejemplo a los directivos y profesores de los distinto establecimientos de nivel primario y secundario de la localidad._

_Además, esto quiero acotar, docentes en especial de mi establecimiento Escuela Normal Almirante Guillermo Brown, quienes al ver por redes sociales fotos de reuniones y del trabajo, se acercaron particularmente para sumarse a esta tarea que se estaba realizando._

· **_¿En qué sectores de la localidad se está llevando a cabo el arborizado?_**

_En distintos lugares o zonas de nuestra ciudad. En la respuesta anterior mencioné que contaba con el apoyo de distintas instituciones, en una de las reuniones de coordinación nos distribuimos las zonas. Es decir, cada establecimiento o institución de nivel primario y secundario seleccionó una zona determinada para la realización del relevamiento de datos acerca de las especies que se encuentran y en qué condiciones se las encuentran, ya que con el proyecto también se pretende renovar aquellos árboles que tienen muchos años de antigüedad. Hay árboles que tienen hasta 100 años y debido a su antigüedad ya implican un riesgo para la comunidad._

_Hay otras instituciones que se encargaron de analizar en otras zonas si es o no necesario arborizar o bien si se necesita o no la renovación de algunas especies en particular._

_Para seguir leyendo esta nota realizada por el Profesor en Cs. Químicas y del Ambiente, **Jonathan Alexander Paredes.** accede a este link:_

[_https://drive.google.com/file/d/18suvcNt1OZeE-pEx8fUrWkA-GQOmmNX9/view?usp=sharing_](https://drive.google.com/file/d/18suvcNt1OZeE-pEx8fUrWkA-GQOmmNX9/view?usp=sharing "https://drive.google.com/file/d/18suvcNt1OZeE-pEx8fUrWkA-GQOmmNX9/view?usp=sharing")