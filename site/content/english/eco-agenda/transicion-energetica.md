---
title: La transición energética, un proceso en marcha en Corrientes
date: 2020-10-02T20:00:00.000+00:00
author: Luciana Tassano
image_webp: "/images/placa-e.jpg"
image: "/images/placa-e.jpg"
description: Corrientes es la provincia del NEA que más aporta en energías renovables;
  la emergencia de la pandemia obliga a poner la mirada en esa dirección.

---
_Corrientes es la provincia del NEA que más aporta en energías renovables; la emergencia de la
pandemia obliga a poner la mirada en esa dirección._

Corrientes viene realizando una tarea destacable en materia de transición energética. El
gobierno de Gustavo Valdés se pone a la cabeza de políticas públicas para promover la
modernización de la industria y la adopción de tecnologías basadas en energías renovables, lo
que se traduce en empleo e innovación. Esto a pesar del contexto adverso que atraviesa
Argentina, con un estado al borde del default, inflación estructural y desequilibrios
macroeconómicos que proyectan una crisis que se agudizará en los próximos meses.

Corrientes es la provincia que más ascendió en el ranking del Índice Provincial de Atractivo
Renovable (IPAR) en 2019. El segundo informe del IPAR refleja que la provincia escaló ocho
posiciones en el listado basado en este indicador que mide, a través de 28 variables, tanto el
grado de desarrollo de las energías renovables en cada jurisdicción del país junto al potencial
atractivo para futuras inversiones. Corrientes está en el noveno lugar y es la provincia del NEA
mejor ubicada, por encima de Chaco (10), Misiones (19) y Formosa (20).

En los últimos años se han instalado en la provincia, sobre todo, plantas de biomasa. Este tipo
de proyectos recibió un fuerte estímulo de la mano de Renovar, un programa del Gobierno
nacional implementado en 2016 que fomenta la inversión en tecnologías renovables a través de
beneficios fiscales y del Fondo para el Desarrollo de Energías Renovables (FODER), que ofrece
financiamiento y garantías para los proyectos.

### **Proyectos en marcha**

Entre los emprendimientos locales se destaca la planta de biomasa de las empresas Kuera y
CAMMESA ubicada en la localidad de Santo Tome. La obra generará más de 200 fuentes de
trabajo en la zona con una inversión cercana a los 50 millones de dólares y producirá 16
megawatts de energía a partir de los desechos de madera.

Otro proyecto importante es la central térmica de biomasa Santa Rosa, ubicada en el parque
forestoindustrial de esa localidad, que se prepara para su inminente puesta en marcha. La
capacidad de producción será de 18 megawatts, de los cuales 15,4 megawatts son exportables al
sistema de CAMMESA y el resto se destinará a la red de energía de la localidad y zonas
vecinas. Esta central potenciará el desarrollo de toda la industria maderera de la región y la
calificación de empleo local. Con una superficie de ocho hectáreas, se calcula que consumirá
unas [20.000 toneladas mensuales de residuos de biomasa generado por los aserraderos de la
zona](https://www.argentinaforestal.com/2020/04/08/genergiabio-corrientes-reorganiza-su-puesta-en-marcha-y-espera-pasar-la-pandemia-para-iniciar-las-pruebas-de-la-nueva-planta-de-generacion-de-energia-de-biomasa-forestal-en-santa-rosa).

En Gobernador Virasoro se encuentra en construcción una segunda planta de energía renovable
del Grupo Insud, que comenzará a operar en 2021 y generará 37 megawatts de energía eléctrica
a partir de biomasa forestal. Se abastecerá de los subproductos de la cosecha y la industria
forestal, como chips, cortezas y aserrín, que en la actualidad no tienen un uso industrial.

El proyecto FRESA —la primera planta del Grupo Insud en Virasoro— es la mayor inversión
forestal de la región destinada a una planta de generación de energía a partir de biomasa
forestal. Generará [40 megawatts de potencia que se volcarán al servicio eléctrico a cargo de la
Compañía Administradora del Mercado Mayorista Eléctrico (CAMMESA). Al día de hoy
cuenta con 250 operarios](https://www.argentinaforestal.com/2019/11/26/virasoro-fresa-la-nueva-planta-de-bionergia-inyectara-energia-renovable-al-tendido-electrico-nacional-en-la-primera-quincena-de-diciembre/).

### **Incentivos nacionales**

Los proyectos mencionados ubicados en la Provincia de Corrientes, corresponden a las rondas
licitatorias enmarcadas en el programa RenovAr que el Gobierno Nacional viene
implementando, en especial desde 2015, para promover las energías renovables. Este plan
incentiva la diversificación geográfica, estableciendo cupos por región para cada licitación.
Hasta el momento se realizaron cuatro rondas, todas sustentadas en el Régimen de Fomento
Nacional para el uso de Fuentes Renovables de Energía (ley 27.191). En total, 191 proyectos
fueron adjudicados a través de estos programas, provenientes de las siguientes fuentes de
energía: 44 de energía eólica, 55 solar fotovoltaica, 20 de biomasa, 46 de biogás, 6 de biogás
relleno sanitario y 20 de pequeños aprovechamientos hidroeléctricos. Se calcula que gracias a
estos proyectos se evitan 9 millones de toneladas de dióxido de carbono al año, lo que equivale
a las emisiones de 1,6 millones de autos.

En la misma línea se encuentra el programa de** Proyectos de Energías Renovables en Mercados
Rurales (PERMER)**, que tiene como objetivo brindar acceso a la electricidad, a partir de
energías renovables, para la población rural dispersa del país. Este programa se sostiene con
financiamiento Banco Interamericano de Desarrollo (BID) y Instituto Arquitectónico de Japón
(AIJ, por sus siglas en inglés). Comenzó con la primera etapa entre los años 2000 y al 2012. El
proyecto —que está en la segunda etapa— fue innovador y tuvo un gran impacto social. En el
marco del PERMER se han concluido hasta el momento 30.000 instalaciones de sistemas
fotovoltaicos, eólicos y termosolares en [19 provincias. Esto benefició a más de 25.000 hogares
rurales, 1.800 escuelas y 350 instituciones de servicios públicos que están alejadas de la red
eléctrica nacional](https://www.argentina.gob.ar/produccion/energia/permer).

En Corrientes comenzó la implementación del proyecto en el año 2006 con la electrificación de
86 escuelas rurales, a través de la instalación y puesta en marcha de sistemas fotovoltaico y
continuó más tarde, con la electrificación de viviendas. A la finalización de la [primera etapa](https://permer.se.gob.ar/contenidos/verpagina.php?idpagina=3726) del
proyecto, diciembre de 2012, se habían realizado 1.300 instalaciones en viviendas y 85 en
escuelas.

Total de Instalaciones

![](https://gitlab.com/Amin-Farina/imagenes/-/raw/master/Politica/eco-agenda-1.jpg)

(*) Provisión de sistemas térmicos.

En el marco de la segunda ronda PERMER, la Secretaría de Energía de Corrientes continúa con
la instalación de paneles solares residenciales autónomos para 700 usuarios rurales distribuidos
por toda la Provincia. Corrientes es unas de las provincias beneficiarias del programa con mayor
cantidad de escuelas incluidas, con un total de 72, que en esta oportunidad recibirán los
equipamientos e instalaciones para contar con energía solar fotovoltaica.

Por último, cabe mencionar la ley nacional 27.424 declara de interés nacional la generación
distribuida de energía eléctrica a partir de fuentes renovables. La generación distribuida tiene
como destino al autoconsumo y la inyección de excedentes en la red pública de distribución.
Tiene como fin acelerar el proceso de adaptación de la matriz energética nacional a los
requerimientos del proceso de transición energética. Asimismo, busca reducir los
requerimientos de fondos públicos para la transformación de la matriz energética, ya que dichas
inversiones serian afrontadas por el propio prosumidor, y abrir líneas de crédito público o
privado. Corrientes cuenta con su Ley Provincial de generación distribudida número 6.428, que
permite la adhesión de la jurisdicción a la ley nacional 27.191.

### **Los desafíos de las renovables en la pandemia**

Las energías renovables tienen dos atributos principales: la renovabilidad, que es una
característica física, y la sustentabilidad, relacionada a los procesos de su aprovechamiento.
Pensar la matriz energética futura del país con estos dos criterios tiene como propósito poner en
el centro la solidaridad y la responsabilidad intergeneracional. No solo se trata de pensar los
beneficios energéticos y económicos, sino los impactos sociales, ambientales y ecosistémicos,
presentes y futuros.

La coyuntura por la COVID 19 nos enfrenta a una reflexión cada vez más profunda acerca de cómo va a recuperarse la sociedad en general y la economía en particular. Para cumplir con los objetivos del Acuerdo de París, los Objetivos de Desarrollo Sostenible (ODS) y los futuros que asumamos con las metas de emisiones de carbono comprometidas, debemos orientar nuestra matriz hacia las energías limpias y renovables, analizando todo el ciclo de vida de la energía de que se trate, tanto como de los equipamientos necesarios para su uso y los procesos industriales y comerciales para su puesta a disposición en obra.

**Luciana Tassano.**

_Abogada. Mg. Derecho Tributario_