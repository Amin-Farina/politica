+++
author = "Jorge Vara"
author2 = ""
date = 2020-10-27T20:00:00Z
description = ""
image = "/images/placa-e2.jpg"
image_webp = "/images/placa-e2.jpg"
title = "Desarrollo Productivo de la Provincia de Corrientes"

+++
En una breve entrevista brindada por el **Diputado Nacional Ing. Jorge Vara** nos cuenta sobre la Agenda de **_Desarrollo Productivo de la Provincia de Corrientes_**, que está compuesto de 6 cadenas estratégicas importantes: Forestación, Ganadería, Arroz, Citricultura, Horticultura, Yerba y té.

Cada una tiene diagnósticos distintos, problemas distintos y soluciones diferentes.

Para ver la Nota completa ingresá en: [Eco Agenda - Diputado Nacional Jorge Vara](https://www.youtube.com/watch?v=nlcp-cPzux8&t=439s "Eco Agenda - Jorge Vara")

**Ganadería:** “Un tema clave es **mejorar los indicadores productivos**, por eso se trabajó en un Plan ganadero. Antes desde Corrientes no se podía enviar carne a otras provincias; hoy tenemos 5 plantas de las cuales 2 están autorizadas para exportar, no solo hacia otras provincias sino también hacia otros países”.

**Citrus: “Se inauguraron dos plantas de jugos en la costa del Río Uruguay**, de gran capacidad y de última tecnología. Se complementó con inversiones y **promoviendo líneas de financiamiento para pequeños productores y cooperativas específicamente en línea de fríos,** lo que permite al productor guardar las frutas para luego salir al mercado y no generar de golpe una sobreoferta y vender a precios viles”.

**Arroz:** “Teníamos una capacidad industrial de las 2/3 partes de la provincia y vamos rumbo a retomar las 100 mil hectáreas de producción, si se mantienen condiciones del mercado externo, ya que **tenemos un 120 % de capacidad industrial**, podríamos crecer un 20% en materia prima e industrializarla”.

**Yerba: “Las dos principales marcas en Argentina son correntinas,** logramos comercializar el 50% de la materia prima, quiere decir que tenemos mucha más industria que producción primaria. **El único gran problema es la logística, es decir los altos costos de los traslados sobre todo a lo que hace a la exportación;** hay que trabajar fundamentalmente en el tema de reactivar los ferrocarriles como solución para el traslado de grandes cantidades particularmente sobre la costa del Río Uruguay y la ruta 14. Esto nos daría mayor competitividad en la región”.

**Foresto industrial:** “Se ha avanzado mucho en la energía, hoy producimos casi un 25% de la demanda de energía de la provincia, **pero solo con los residuos foresto-industriales, sin talar un solo árbol, de la industria existente podríamos generar el 100% de la energía consumida en nuestra provincia,** para uso doméstico, para industria o para alumbrado público. **Generando energía limpia y una gran contribución de la provincia de Corrientes en la lucha y prevención contra el cambio climático”.**

**Industria celulósica:** “Es un tema clave, estamos hablando de grandes inversiones que podrían cambiar la realidad de nuestra provincia. En este sentido **presenté un proyecto teniendo en cuenta el uso de las mejoras tecnológicas a nivel mundial, con el objetivo de cuidar el medioambiente, cuidando al medio ambiente, a los ciudadanos de la provincia y cuidando a las futuras generaciones, quienes seguirán generando actividad en nuestra querida provincia**”.