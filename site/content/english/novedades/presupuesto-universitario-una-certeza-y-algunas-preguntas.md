+++
author = "Emilio Cornaglia"
author2 = ""
date = 2020-10-26T18:00:00Z
description = ""
image = "/images/placa-n3.jpg"
image_webp = "/images/placa-n3.jpg"
title = "Presupuesto Universitario, una certeza y algunas preguntas."

+++
Tras un arduo debate en comisiones, este miércoles se votará en Diputados el Proyecto de Presupuesto para el 2021. El mismo fue elevado el 15 de Septiembre por el Poder Ejecutivo, en un contexto de incertidumbre y crisis económica que no encuentra techo. Sus estimaciones sobre cuestiones centrales como la inflación y los ingresos son tan poco serias que casi nadie -salvo el oficialismo y sus adláteres- le otorga credibilidad a la que debiera ser la “ley de leyes”. Ayer se aprobó el dictamen oficialista en Diputados sin el acompañamiento del Bloque de Juntos Por el Cambio. Sin embargo, la maquinaria de la política oficial está abocada a conseguir la aprobación del Presupuesto con el mayor nivel de consenso posible.

Más allá de estas disquisiciones, en materia de Presupuesto Universitario existe una certeza: los fondos asignados al sistema de universidades públicas apenas alcanzarán para cubrir los gastos de funcionamiento y el pago de salarios en 2021. Esto viene a profundizar el durísimo panorama de asfixia presupuestaria que debieron afrontar durante este año, con un presupuesto 2019 reconducido y una inflación del 87% acumulada hasta el mes de septiembre 2020. De esta manera, la misión de las Universidades Públicas se verá fuertemente comprometida debido a este ajuste presupuestario encubierto por inflación.

Los fondos para las Universidades Nacionales figuran en el programa para el “Desarrollo de la Educación Superior”, el cual destina $230.171 millones para 2021, mostrando un aumento de 16,94%,en comparación al crédito vigente al 18 de octubre ($196.821 millones). Si consideramos que el Proyecto de Presupuesto prevé un 29% de inflación para el 2021, con un dólar “oficial” a 103$, está a la vista que las Universidades tendrán menos recursos que los que necesitarán. El asunto es más grave aún si tenemos en cuenta que el “dólar libre” cerró ayer a 195$.

El total de los fondos destinados a Universidades Nacionales establecido en el Art. 12 de la Ley de Presupuesto se detalla en una Planilla Anexa llamada “planilla A”. Los montos de esa Planilla fueron parte de las modificaciones de último momento. En la misma $215.302 millones se distribuyen taxativamente entre las 56 Universidades pero llama la atención que $9.728 millones quedan en Programas “bajo la línea”: estos son fondos que la SPU podrá distribuir de manera discrecional.

En pleno debate parlamentario, apareció la famosa “Planilla B” -anexa al Art. 12-, con la que se distribuyen más fondos para las Universidades. En total esta Planilla reparte $4.220 millones. Alguno podría pensar que dicho monto bien pudiera destinarse a compensar el desequilibrio en la afectación de fondos que hoy castiga a grandes Universidades en beneficio de algunas con afinidad kirchnerista, pero lamentablemente sus números acompañan la tendencia de una distribución que privilegia la “construcción política” por sobre el respeto a la autonomía y la autarquía universitaria. Esta planilla será objeto de negociaciones hasta último momento antes de votarse el Proyecto, por lo que aún hay final abierto.

Por otro lado -en la jurisdicción Obligaciones a Cargo del Tesoro (Jur. 91)- aparecen como “transferencias a Universidades Nacionales” nada menos que 40.000 millones, los cuales están destinados a “Asistencia Social”. Estos fondos implican el 14,5% del total asignado a las Universidades. Si fuéramos políticamente correctos podríamos decir que esto es una suma muy significativa de dinero que irá a asistencia social para ser transferida a las Universidades Nacionales, pero en honor a la verdad vamos a decir que es una suma descomunal de fondos cuyo destino quedará a exclusivo criterio del Gobierno Nacional. Comparativamente, representa cuatro veces el presupuesto de la Universidad Nacional de Rosario o casi 3 veces el presupuesto de la Universidad Nacional de Córdoba.

En conclusión, a la preocupación por el recorte de fondos para las Universidades, se suma la concentración de fondos en manos de la SPU o la Jefatura de Gabinete: si sumamos los fondos del Art. 12 (planillas A y B), los 40 mil millones de la Jurisdicción 91, y algunos otros Programas, el PEN manejará más de $57.172 millones de pesos, lo cual representa un 20,66% de los fondos destinados por el Presupuesto 2021 para Universidades Nacionales. Esta maniobra permitirá al oficialismo distribuir los fondos de cara a un año electoral decisivo, beneficiando a Universidades “amigas” y castigando a aquellas que sean críticas o no se arrodillen ante el Gobierno.

Es importante destacar que esto no sucedió durante el Gobierno anterior: en 2016 los fondos para “Universidades sin discriminar” pasaron del 8,68% del total a un 4,7% del total, para luego bajar a casi cero durante los años siguientes.

![](/images/uunn-sin-discriminar.jpg)

Con el peronismo en el poder, lejos quedaron las celebraciones por el “Centenario de la Reforma” en el 2018 y se diluyó el alto nivel de consenso en torno a los postulados de la Reforma Universitaria. La distribución de esos fondos entre las distintas Universidades Nacionales debería estar dada por el mismo Congreso de la Nación, en base a lo establecido por el Art. 75 inc. 18 de la Constitución Nacional, el cual dice claramente que las Universidades Nacionales son Autónomas y Autárquicas. Además la Constitución también prohíbe en su Art. 76 la delegación legislativa en el Poder Ejecutivo.

Por último, debemos tener en cuenta que según las transferencias mensuales a Universidades Nacionales al 18/10/2020, el saldo disponible es de **$49.144 millones.** Considerando el promedio simple de los últimos 3 meses cerrados, la transferencia mensual rondaría los $17.772 millones. Si tenemos en cuenta que diciembre es un mes de mayor gasto por aguinaldo, queda claro que los $49.144 millones no alcanzarán para terminar el año.

![](/images/presentacion-sin-titulo.jpg)

En un año en el que las Universidades tuvieron que hacer grandes esfuerzos para adaptar toda su oferta académica a la virtualidad, implementar protocolos sanitarios que garanticen las condiciones mínimas para desempeñar el trabajo administrativo, desarrollar programas y acciones para asistir a la sociedad en el marco de la pandemia, concentrar los esfuerzos de investigación y poner a disposición la infraestructura y personal hospitalario para atender las demandas que impuso el COVID-19, la premisa constitucional del Estado garantizando la Educación Superior no se cumplió.

De cara al 2021 hay una certeza: el PEN podrá manejar más de $57.172 millones en forma completamente discrecional. Con el Peronismo en el Gobierno volvió su herramienta predilecta para avasallar la autonomía de las Universidades: el manejo discrecional y arbitrario de fondos para el Sistema Universitario. La pregunta es cómo harán las Universidades para desarrollar su misión sin contar con los fondos necesarios.

_*Este artículo fue escrito por **Emilio Cornaglia** en base a informes técnicos y gráficos producidos por el Lic. A. Nicolás Lorenti. Fuente: Ministerio de Economía de la Nación y Proyecto de Presupuesto 2021 HCDN._