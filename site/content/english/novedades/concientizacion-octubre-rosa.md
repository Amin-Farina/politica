+++
author = "Juventud Radical Corrientes Capital"
author2 = ""
date = 2020-10-10T15:00:00Z
description = ""
image = ""
image_webp = "/images/photo4916133392681117831.jpg"
title = "Concientización Octubre rosa"

+++
Octubre ¿Por qué "mes rosa"?

Es el Mes de la Concientización de la Lucha contra el cáncer de mama.

Nos acercamos a intervenir el monumento La Taragüí, que históricamente representa a las mujeres correntinas, colocando un lazo rosa: gran símbolo de esta lucha, junto al Intendente de la Ciudad de Corrientes Eduardo Tassano, la concejal Florencia Ojeda, y el acompañamiento del comité capital de la Juventud Radical,  
Como jóvenes, nuestro objetivo es visibilizar la importancia de la prevención de esta enfermedad.![](/images/photo4916133392681117832.jpg "Tassano")

![](/images/photo4916133392681117829.jpg)