+++
author = "Héctor Torres"
author2 = ""
date = 2021-01-29T03:00:00Z
description = ""
image = "/images/img_20210129_164004_574.jpg"
image_webp = "/images/img_20210129_164004_574.jpg"
title = "Derechos electorales en pandemia"

+++
**Como garantizar el ejercicio de los derechos electorales en pandemia.**

La pandemia del COVID-19 está obligando a nuestras instituciones a adaptarse a un contexto que es muy diferente de aquel en el que habitualmente desarrolla sus funciones. La organización de las elecciones no es la excepción. En Argentina el año 2021 marca el inicio de un nuevo ciclo electoral. La Provincia de Corrientes va a elegir Gobernador y Vice Gobernador, renovación de la Cámara de Diputados y Senadores de la Provincia, Intendente y Vice Intendente (aquellos municipios que adhieran obviamente) y renovación de Concejos Deliberantes. Estas elecciones se darán en un contexto excepcional en donde el movimiento, la aglomeración y el contacto entre las personas plantean serios interrogantes a la manera en la que se desarrollarán los comicios.

Las elecciones son grandes operativos de acción colectiva y esa coordinación colectiva que implica cada proceso electoral este año se ve desafiada por la irrupción de la pandemia, entonces lo prioritario debería ser que la necesidad de toda adaptación logística preserve condiciones y valores que tienen que ver con la calidad de nuestro sistema democrático: asegurar y garantizar la participación electoral, la igualdad en el acceso y ejercicio del sufragio y la equidad en las condiciones de competencia, el secreto del voto, y el control ciudadano de parte de los fiscales partidarios y de las autoridades de mesa.

La heterogeneidad subnacional en cuanto a las reglas electorales y las diferentes situaciones epidemiológicas de los municipios del interior de la Provincia de Corrientes hacen que las medidas que se implementen para garantizar las elecciones deban tomar en cuenta una serie de factores sin precedentes para la administración electoral como son las conocidas situaciones epidemiológicas municipales.

La posibilidad de que en Corrientes se realicen elecciones no simultáneas con la nacional habilita la eventualidad de que cada estado subnacional provincial pueda establecer sus propias reformas electorales para este momento tan particular y que su implementación traiga efectos específicos en cuanto a la forma de organizar la elección como podría ser reformas relacionadas al sistema de organización del sufragio o métodos alternativos de votación como ser el voto anticipado por ejemplo, ya sea presencial o por correspondencia, con el cual se podría reducir el riesgo del elector a la hora de emitir el voto y disminuir la concentración de personas en los lugares de votación durante la jornada electoral. La expansión de los derechos electorales (voto optativo a personas de 16 años) hace que sea fundamental la igualdad en el acceso al voto,

Así como se deben tomar todas las medidas necesarias para garantizar el derecho electoral a los ciudadanos, es indispensable asegurar la equidad en las condiciones de competencia para todas las agrupaciones políticas que presentan candidatos a los cargos electivos en juego.

Enumeramos una serie de medidas como una manera de garantizar el proceso electoral:

**1) Generar e implementar mecanismos de consulta y comunicación interinstitucional:**

Armar grupos de trabajo conjunto conformados por autoridades electorales, sanitarias, responsables de la logística electoral (correo, fuerzas de seguridad, etc.) con los partidos y agrupaciones políticas porque la opinión de los dirigentes partidarios son fundamentales para la legitimidad del sistema y para que la formación de un espacio de diálogo promueva el debate abierto e informado construyendo una mayor confianza política. Diseñar protocolos de actuación junto a las autoridades sanitarias provinciales que tomen en cuenta la situación epidemiológica local y garanticen la integridad de los procesos y el pleno ejercicio de los derechos políticos.

**2) Aportes referidos a los momentos previos a la elección:**

Considerar reformas en la normativa vigente referida a los plazos que rigen en los calendarios electorales, establecer procedimientos electrónicos (plataformas de trámites a distancia) para la presentación de escritos e impugnaciones, generando información y capacitaciones sobre el uso de estas herramientas tecnológicas, atención y reuniones presenciales con citas previas para evitar acumulación de personas, cumpliendo siempre con el distanciamiento y así asegurar que todas las personas puedan acceder a estas instancias, con independencia de su conectividad, promover el uso de herramientas tecnológicas para la inscripción de candidaturas, el reconocimiento de alianzas transitorias para participar en los comicios. Evaluar la posibilidad de implementar nuevos procedimientos que garanticen que la población de riesgo en edad de votar,

**3) Aportes referidos al período de campaña**

Organizar una reunión con representantes de partidos políticos para generar consensos y acordar en conjunto las mejores alternativas y recomendarles no organizar reuniones masivas, sino optar por medios de comunicación, redes sociales y plataformas virtuales.

Alternativamente, se podría pensar en un diseño que permita la realización de algunos actos de campaña, como foros informativos con distanciamiento social, mientras que otros que requieren de un contacto físico más cercano, como los volanteos, podrían limitarse (de manera transitoria) o incorporar medidas sanitarias como el distanciamiento social y demás métodos de prevención para actividades presenciales (tapaboca, alcohol en gel, etc.). Asimismo, la realización final de los actos y formatos de campaña puerta a puerta siempre debe estar supeditado a las disposiciones de la autoridad sanitaria y respetar las medidas de aislamiento o distanciamiento social, según corresponda.

Las elecciones son un acontecimiento sustancial de las demócratas y también es una actividad colectiva en el cual ejercemos nuestros derechos electorales. Los escenarios de pandemia y pos pandemia obligan a repensar y rediseñar los diferentes aspectos del acto electoral y la logística eleccionaria tal como los conocemos por lo tanto todas las elecciones deberán adaptar sus procesos para garantizar la integridad de los comicios, el ejercicio de los derechos cívicos y políticos de los votantes, y asegurar en el proceso las condiciones sanitarias que impone la pandemia.

**Héctor Torres**

**Lic. en Ciencias Políticas.**