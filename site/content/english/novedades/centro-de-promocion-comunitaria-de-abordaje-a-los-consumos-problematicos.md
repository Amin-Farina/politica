+++
author = "Maira Andruchovicz"
author2 = ""
date = 2022-09-18T20:00:00Z
description = ""
image = "/images/placa-n12-maira.jpg"
image_webp = "/images/placa-n12-maira.jpg"
title = "Centro de promoción comunitaria de abordaje a los consumos problemáticos."

+++
Un equipo interdisciplinario de profesionales de la Ciudad de Corrientes, con su trabajo **_“Andar y Desandar: Construyendo caminos en un Centro de Promoción Comunitaria para el Abordaje a los Consumos Problemáticos”,_** participó en el XV Congreso Argentino de Salud Mental y también, en el V Congreso Argentino de Abordaje Interdisciplinario a los Consumos Problemáticos.

El equipo está compuesto por las Licenciadas Maira Belén Andruchovicz, María José Velozo Lencinas, Analía Itatí Martínez, Fernanda Saravia, Martin Alejandro Ayala Ortiz y Evangelina Aquino, todas profesionales de diferentes disciplinas trabajando en el Plan Provincial “Más Vida”, mediante el cual se integran estrategias de Promoción, Prevención y Asistencia al Consumo Problemático desde un abordaje integral y comunitario en la temática.

La elaboración del presente manual nace como propuesta de acción para que los equipos puedan acceder a este documento ya que el mismo funciona como brújula para posibilitar herramientas que permitan re-pensar las prácticas realizadas hasta el momento.

_"Respecto a la importancia del trabajo que estamos haciendo, radica en que como profesionales del Primer centro de promoción comunitaria de abordaje a los consumos problemáticos de la provincia, **construir una guía que sea de referencia para los demás centros del plan mas vida de la provincia**, conteniendo experiencias practicas de abordaje, marcos teóricos, y considerando siempre las normas legales que rigen en materia de derechos en nuestro país pero también la idiosincrasia local para hacer un abordaje adecuado de la tematica._

_A su vez, una guía como esta, ayuda a repensar nuestras prácticas, hacer nexo con el interior de la provincia, crear puentes donde no los hay, manejar un discurso y lenguaje común entre todo el personal que está involucrado directa o indirectamente en el abordaje de la problemática"._

Maira Andruchovicz, profesional del Plan Provincial + Vida.-