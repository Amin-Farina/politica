+++
author = "Noel Eugenio Breard"
author2 = ""
date = 2020-10-02T21:00:00Z
description = "Por iniciativa del bloque UCR, se dio media sanción a la reedición del libro “Memoria para los Anales de la Provincia de Corrientes”"
image = "/images/placa-n2.jpg"
image_webp = "/images/placa-n2.jpg"
title = "Pedro Ferré, padre del Federalismo"

+++
En la 10° Sesión de la Cámara de Senadores, por iniciativa del Bloque Unión Cívica Radical, se dio media sanción a la reedición del libro “Memoria para los Anales de la Provincia de Corrientes” de Pedro Ferré, edición original de 1921.

Con este proyecto se busca distribuir el ejemplar en bibliotecas públicas, institutos y museos de todo el territorio nacional y provincial, además de ser exhibida en eventos culturales como un mensaje histórico y de futuro que indique que **las luchas que se libraron históricamente por el federalismo siguen vigentes y tienen sentido en el futuro.**

_“Pedro Ferré fue el padre del Federalismo en la Argentina, fue quien dio el debate sobre este tema”_, sostiene el **Senador Provincial Noel Eugenio Breard**. Observa más del fundamento [Aquí](https://www.facebook.com/1381284145444517/posts/2678340662405519/) en este video.

[Presiona aquí para ver el proyecto de ley completo.](https://drive.google.com/drive/folders/1SZjrvXiNeHXX1KJii6AhXDvXI6ZbJvN5?usp=sharing)