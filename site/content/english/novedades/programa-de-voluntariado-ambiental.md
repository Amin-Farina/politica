+++
author = "Florencia Ojeda"
author2 = ""
date = 2020-10-26T03:00:00Z
description = ""
image = "/images/placa-n4.jpg"
image_webp = "/images/placa-n4.jpg"
title = "Programa de Voluntariado Ambiental"

+++
**Creación del “Programa de Voluntariado Ambiental”**

El objetivo principal es la educación, promoción y concientización ambiental.

Buscamos disminuir el impacto negativo del ser humano sobre el medio ambiente, en especial en nuestra región, mediante la participación de voluntarios en el cuidado ambiental y el control de la contaminación.

Todos debemos ser actores del cambio. Con acciones concretas, como la limpieza de espacios públicos y eventos públicos masivos, la formación, capacitación y promoción de proyectos, se pretende constituir espacios de trabajo, articular en red con los voluntarios ambientales y sensibilizar a toda la sociedad de forma progresiva.

Es un proyecto presentado en el Concejo Deliberante de Corrientes a reglamentarse por el Poder Ejecutivo de la Ciudad.

[t_video4952046865076977882.mp4](/images/t_video4952046865076977882.mp4 "Programa Voluntariado Ambiental") 