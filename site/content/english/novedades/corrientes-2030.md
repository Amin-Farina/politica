+++
author = "Gustavo Adolfo Valdés"
author2 = ""
date = 2020-10-15T15:00:00Z
description = ""
image = "/images/photo4918385192494803108.jpg"
image_webp = "/images/photo4918385192494803108.jpg"
title = "Corrientes 2030"

+++
Te invitamos al lanzamiento del Programa "Corrientes 2030", en el marco del Plan Estratégico Participativo (PEP).

Sus políticas de regionalización, descentralización y fortalecimiento institucional de municipios, es una de las iniciativas más importantes del gobierno de Corrientes.

Seguí la transmisión en vivo por la página de facebook @GustavoValdesOk el viernes 16/10 a las 10hs.