+++
author = "Martin Tognola"
author2 = ""
date = 2021-09-21T20:00:00Z
description = ""
image = "/images/placa-n9-martin-nuevo.jpg"
image_webp = "/images/placa-n9-martin-nuevo.jpg"
title = "La sociedad argentina emitió su voto de confianza y esperanza."

+++
**Derrota electoral con magnitudes históricas.**

La sociedad argentina expresó y canalizó la enorme desilusión y decepción respecto a la gestión del gobierno nacional

que se caracteriza por los abusos de poder y decisiones autoritarias que condicionan el ejercicio de las libertades individuales, limitando los principios fundamentales de nuestro sistema republicano y democrático, un plan gubernamental ineficaz que insistió en instalar en la agenda pública una falsa dicotomía entre salud y economía, y cometió gravísimos errores en el manejo de la gestión sanitaria de la pandemia.

La frase del máximo mandatario nacional llama poderosamente la atención, admitió: “Algo no habremos hecho bien para que la gente no nos acompañe”. Señor Presidente, ¿es necesario que se le refresque la memoria?: vacunatorios vip, fiestas clandestinas en Quinta de Olivos, crecimiento del narcotráfico, doloroso aumento de la pobreza y desempleo, deterioro del sistema educativo, cierre de miles de pequeñas y medianas empresas, capital extranjero que decidió no invertir más en el país, inflación asfixiante que incide en la pérdida del poder adquisitivo de la población, el MSCI ( Morgan Stanley Capital Index) anunció que la economía argentina abandonó la categoría de mercado emergente (compartiendo lugares con Trinidad y Tobago, Zimbabwe, Libano y Palestina, entre otros), volvimos a figurar en el Top 10 del Índice de Miseria Económica Global (rodeado por naciones con graves conflictos bélicos y civiles), una penosa política exterior que decidió retirar la denuncia contra el régimen de Maduro por la violación a los derechos humanos y se abstuvo en la votación por la que la OEA condenó el arresto de precandidatos presidenciales en Nicaragua, y lo más reciente, la divisa argentina se sitúa en la sexta ubicación de las monedas más devaluadas del mundo.

Seguramente habré omitido información, pero me parecen más que certeros y razonables todos los datos que mencioné, como causas principales de la derrota electoral con magnitudes históricas, ya que perdieron las PASO en 17 provincias, y para poder comprender la dimensión de este suceso, **el peronismo reunido fue derrotado por primera vez desde los tiempos de Raúl Alfonsín**.

Es momento de tener el convencimiento de que es una falacia decir y defender la idea de que el “peronismo unido, jamás será vencido”. Los hechos recientes así lo demuestran.

Respecto a nuestra provincia, los correntinos afianzaron su voto de confianza al elegir la Lista Verde 502A, boleta que reúne y representa a hombres y mujeres comprometidos con defender los intereses de Corrientes e impulsar su desarrollo socioeconómico, en el Congreso de la Nación.

Dimos el primer paso fundamental que nos posiciona favorablemente para las elecciones legislativas próximas, a realizarse el 14 de noviembre.

Tenemos la oportunidad propicia de decir basta al populismo que tanto daño causó a nuestra nación, basta al autoritarismo nefasto, basta a la hipocresía y cinismo, basta de no hacerse cargo de los errores propios y de culpar siempre a los demás.

Y también, la oportunidad de recuperar la esperanza y de apostar por un proyecto de país viable para todos los ciudadanos argentinos. Una República con estabilidad económica, seguridad jurídica, calidad institucional y solidez democrática. Y por último pero no menos importante, un país donde los jóvenes no sientan que Ezeiza es su única salida.

Martin Tognola.  
Profesor de Educación Superior en Ciencias de la Educación.-