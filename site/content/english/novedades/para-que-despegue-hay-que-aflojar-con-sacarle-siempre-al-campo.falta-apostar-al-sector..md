+++
author = "Agustina González Davis"
author2 = ""
date = 2021-05-21T22:00:00Z
description = ""
image = "/images/placa-n8-agus.jpg"
image_webp = "/images/placa-n8-agus.jpg"
title = "“Para que despegue, hay que aflojar con sacarle siempre al campo.  Falta apostar al sector.”"

+++
**Economía:**

El campo, miles de personas emprenden, se arriesgan, se bancan la burocracia pero es el estado el que se encarga de poner trabas que hacen el trabajo más difícil.

El campo es el 20% del PBI, el 60% de las exportaciones y eso equivale a unos U$S 30 mil millones de dólares anuales, 3 millones de empleo.

**El campo es un motor en marcha queriendo empujar la economía pero que está frenado, sin embargo, siempre ha demostrado estar a la altura de empujar el desarrollo de la Argentina** y que cuando lo dejan, con tan solo levantar un poco el pie del freno, demuestra que es capaz de aumentar la producción, aumentar las exportaciones, aumentar la inversión. ¿Por que el freno? por políticas de corto plazo, malas decisiones del gobierno y sin embargo cuando hubo baja de retenciones o se apostó a la infraestructura, el campo pudo avanzar y acelerar su velocidad.

Cada proceso lleva su tiempo, no es cierto que uno tira una semilla al suelo y sale una cosecha. Uno tiene que acondicionar la tierra, fertilizarla, combatir las plagas, cosechar, transportar e industrializarla.

Dato: La agroindustria produce insumos reclamados en el mercado mundial y el Estado se queda con más del 60% de las ganancias con el cobro de retenciones.

Obstáculos: 160 impuestos que te sacan las ganas.

Argentina tiene un PBI fuera del país, que hay que hacer que vuelva. ¿Cómo? Generando estabilidad macroeconómica, reglas de juego claras.

**Para que despegue, hay que aflojar con sacarle siempre al campo. Falta apostar al sector.**

“El campo es el motor de nuestra industria”, afirmó orgulloso Eduardo Borri, presidente de la empresa oriunda de Marco Juárez, Metaflor. “Muchas veces se lo estigmatiza diciendo que no genera trabajos directos, pero el efecto derrame que tiene en la economía es impresionante”.

Quienes conocen de cerca la actividad no dudan en afirmar que el sector está a la altura de los mejores del mundo. Lo que le falta es el ámbito institucional y que las reglas no vayan en contra de la inversión.

El productor agropecuario está siempre preparado para lo peor y no solo estamos hablando del clima. En la Argentina, además, tiene la permanente incertidumbre de que el estado proponga medidas que no lo comprenda y en cambio lo ahogue. El campo tiene un potencial que se ve y al que el estado recurre en situaciones de crisis, por eso sigue estando entre los mejores sectores. Deberíamos entender que el campo es el único sector que no puede levantarse de la mesa e irse en el medio de la discusión.

Los trabajadores no pierden la esperanza de que el país sea algún día lo que ellos creen que podría llegar a ser. Tenemos todas las condiciones para salir adelante: talento, buena educación y riquezas naturales.

En los últimos días, el Poder Ejecutivo cerró las exportaciones por 30 días y el Presidente dijo “Hasta el año 45 la Argentina tenía dos millones de cabezas de ganado y cuando llega el peronismo ese número empieza a crecer. Cuando el peronismo se va, deja 3 millones de cabezas de ganado. ¿Cuántas cabezas de ganado tenemos hoy? Tres millones de cabezas.. Desde el año 55 no hemos aumentado la cantidad de cabezas de ganado, lo que sí aumentó es la cantidad de habitantes".

Ese es el stock bovino en la Argentina, 3 millones de cabezas de ganado, cuando en realidad supera los 54 millones de cabezas según los últimos números aportados por el Servicio Nacional de Sanidad y Calidad Agroalimentarios (Senasa). En Argentina hay 54.460.799 cabezas.

Sostener que en Argentina hay tres millones de cabezas, no resiste el análisis, no hay lógica y nos coloca en la dolorosa y preocupante circunstancia de observar asombrados, que **quienes dirigen nuestros destinos, nuestro país, no cuentan con los conocimientos suficientes, o están mal asesorados por lo tanto, es posible esperar que sus decisiones sean siempre equivocadas.**

Dato: El cierre de exportaciones de carne por 30 días implicará la pérdida para el país de unos 250 millones de dólares y tal vez, la pérdida de mercados.

¿Van a dar marcha atrás con la medida, sabiendo que este mismo método aplicado en el gobierno de Néstor Kirchner en el 2006 no dio resultado?

**Espero que la razón se imponga a la ideología.**

**Espero que la memoria se imponga a la confrontación.**

**_Agustina González Davis -._**