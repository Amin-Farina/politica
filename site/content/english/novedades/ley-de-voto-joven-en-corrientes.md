+++
author = "Diógenes González"
author2 = ""
date = 2022-09-07T11:00:00Z
description = ""
image = "/images/placa-n11-diogenes.jpg"
image_webp = "/images/placa-n11-diogenes.jpg"
title = "Ley de Voto Jóven en Corrientes"

+++
Estamos asistiendo junto con el voto de paridad que se consiguió en la Cámara de Diputados, a la consagración de ampliación de derechos.

Particularmente debo destacar el hecho de que los legisladores que fuimos elegidos entre las elecciones del año 2019 y el año 2021 hicimos campañas con estos proyectos referidos a paridad y a voto joven. Me parece que unas de las cosas que tenemos que hacer quienes nos postulamos en las elecciones y participamos como candidatos es honrar nuestros compromisos y eso es exactamente lo que estamos haciendo, cumpliendo nuestro compromiso asumido frente a la ciudadanía, y en este caso particularmente frente a los jóvenes.

**Los jóvenes han sido protagonistas de la historia permanente** y por supuesto que es una referencia innegable, pero antes del 2003 pasaban cosas también en la política. Una persona intachable como Leandro Alem supo albergar la Unión Cívica de la Juventud hace más de 130 años, y fundó un partido político que estaba basado en la credibilidad y en la ejemplaridad de su líder.

**La política argentina se ha conformado en los distintos partidos políticos siempre por líderes fuertes** y una enorme cantidad de jóvenes que han arraigado determinados proyectos políticos, asique han sido ellos los que han visibilizado las grandes transformaciones del siglo XX y los movimientos políticos que se están desarrollando en el siglo XXI.

Son los jóvenes los que hoy nos interpelan, justamente son los menores de 18 años los que se quieren ir del país, porque ven la situación económica, la falta de ejemplaridad, las discusiones del Poder Judicial, los temas en los medios nacionales, parece la Argentina entrampada en una discusión estéril sobre a quién le toca ser procesado. La verdad que ese panorama sombrío hace que nuestros jóvenes estén mirando las embajadas.

Asique hoy, nosotros al revindicar este voto, lo que estamos haciendo es abrir una ventana para escuchar con crudeza lo que piensan los jóvenes. **La agenda política correntina tiene que tener un oído especial para escuchar a esos jóvenes** que tienen problemas, que quieren cambios en la educación, que quieren acceder al trabajo, que quieren acceder a la salud, que quieren tener un destino dentro de su país, que están en familias que quieren tener un progreso y dentro de un país en el que todavía las provincias del Norte Grande estamos esperando enormes soluciones y queremos formar parte de un proyecto de Nación.

Asique sin duda que este voto es el **cumplimiento de un compromiso político**, este voto es abrir una nueva ventana en la participación a la ampliación de derechos y a mejorar lo que tiene que ser el vínculo con la sociedad, por todas estas razones el voto es favorable.

Para ver el video de la intervención en la sesión del Senado [https://youtu.be/sI6rIxUZY2I](https://youtu.be/sI6rIxUZY2I "hace click acá")

_Diógenes González_

_Senador de la Provincia de Corrientes.-_