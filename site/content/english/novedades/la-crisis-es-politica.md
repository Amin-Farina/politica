+++
author = "Santiago Merino"
author2 = ""
date = 2022-08-10T03:00:00Z
description = ""
image = "/images/placa-n10-santi.jpg"
image_webp = "/images/placa-n10-santi.jpg"
title = "La crisis es política"

+++
Renuncias, salidas y llegadas al gabinete nacional anunciadas por redes sociales, un presidente ausente y carente de liderazgo, un “superministro” que manejará las relaciones con los organismos internacionales y toda la “rosca” económica, un Presidente del Banco Nación que se entera por whatsapp en medio de una actividad oficial que va a ser reemplazado de su cargo, el riesgo país y la inflación por las nubes, todos los meses más familias se encuentran bajo la línea de la pobreza, el peso argentino cada día vale menos, recesión económica, malestar social, solo para mencionar algunos de los tantos problemas que tenemos y que en el corto plazo este gobierno está muy lejos de solucionar.

Nuestro país se encuentra en **recesión**, hace más de 10 años que no tenemos un crecimiento económico sostenido, condición necesaria para generar desarrollo.

La **inflación**, flagelo de la Argentina históricamente, castiga con mas fuerza a los sectores más vulnerables de la sociedad. Según el INDEC, la inflación interanual en junio de 2022, con un **64%**, es la más alta registrada en los últimos 30 años, y el Indice de Precios al Consumidor (IPC) de marzo del 2022, con un **6,7%,** fue el mayor registrado en los últimos 20 años.

Como si esto fuera poco, ya tenemos una inflación acumulada de **36,2%** (también la más alta registrada en los últimos 30 años), cuando para el mismo período en el 2021, y recién saliendo de la cuarentena estricta por el COVID-19, fue del **25,3%.**

Nos decían que la inflación venía desacelerándose mes a mes, pero ese relato duró casi nada y ya el índice de junio (5,3%) fue mayor al de mayo (5,1%), y el del mes de julio que proyectan las consultoras se acerca al 8%, proyectando la inflación anual a casi un 80% anual.

![](/images/inflacion-anual.png)

En términos **cambiaros**, desde el Banco Central (BCRA), nos dicen que no habrá un salto abrupto del dólar oficial, pero la brecha entre este y el dólar paralelo se disparó a más del doble, generando fuertes expectativas devaluatorias, además, con el aumento de 35% a 45% el recargo para las compras en el exterior con tarjeta de crédito. El “dólar ahorro” se mantiene en USD 200 para la compra mensual, pero con más restricciones a medida que pasa el tiempo.

Respecto al **déficit fiscal**, en las últimas semanas surgieron anuncios por parte de la ya ex Ministra de Economía de la Nación Silvina Batakis, donde lanzaba una serie de medidas para reducir el mismo pero sin un plan económico que nos marque la hoja de ruta a seguir, claramente demostrado en los hechos ya que solo estuvo 24 días en el cargo. Este gobierno, de haber continuado con la reducción progresiva del déficit iniciada por el gobierno anterior, tal vez hoy la situación sería distinta, ya que los períodos en que la Argentina pudo “controlar” la inflación fueron períodos con superávit fiscal.

Con este oscuro panorama económico nos encontramos en la antesala de un año electoral, donde el Gobierno Nacional no encuentra el rumbo y nos tiene de rehenes a todos los argentinos y argentinas con sus peleas internas. Un presidente ausente en medio de un país incendiado, con una vicepresidenta con mucho poder tirando para el otro lado y manejando las áreas con mayor caja y manejo político del gobierno.

Sin planificación, sin rumbo claro, sin instituciones ni un liderazgo fuerte, Alberto Fernández tiene que terminar su gestión dentro de un año y medio. Mientras tanto, los argentinos y argentinas tenemos que soportar la ineficiencia, inoperancia e ineptitud de los que, durante muchísimos años, jugaron con los sueños y esperanzas de desarrollo personal y colectivo.

En 2023 tenemos la oportunidad de generar un cambio de rumbo, ordenando la macro, pero también ordenando una de las mayores causas de esta crisis económica que vivimos, la crisis política.

Cr. Santiago Merino.-