+++
author = "Martin Tognola"
author2 = ""
date = 2021-01-16T20:00:00Z
description = ""
image = "/images/placa-n6-martin.jpg"
image_webp = "/images/placa-n6-martin.jpg"
title = "El estruendoso silencio cómplice del robo a los jubilados."

+++
Siempre me pregunto, ¿hemos naturalizado como sociedad la hipocresía y el cinismo reinantes en Argentina?; ¿la transparencia y la dimensión ética en la política están sepultadas? o ¿directamente se evaporaron?

El 30 de diciembre de 2020, mientras se debatía en el Senado el proyecto de ley de la legalización del aborto, transcurría paralelamente en la Cámara de Diputados, la votación de la nueva fórmula de movilidad jubilatoria.

Hay que admitir que fue una jugada estratégica, lograron eclipsar el ajuste y el robo a los jubilados. Dejémonos de eufemismos y digamos los hechos como verdaderamente son.

Esta iniciativa fue orquestada por el gobierno nacional, que, en plena campaña electoral, mintió vilmente, diciendo que los jubilados eran su prioridad.

La fórmula consiste principalmente en actualizaciones trimestrales que combinan un 50% de la recaudación de Anses y otro 50% de la variación salarial; pero lo más grave es que excluye al índice de la inflación como componente de actualización de los haberes (en plena crisis social, económica y sanitaria), conllevando a una mayor pérdida del poder adquisitivo, de casi 18 millones de personas, entre jubilados, pensionados y beneficiarios de asignaciones sociales.

No hay que tener una mente brillante o ser una eminencia intelectual, para darse cuenta de la insensatez de esta reforma. En un país como el nuestro, que la inflación es un mal endémico, ¿es una decisión acertada exceptuarla de la fórmula de movilidad jubilatoria? En el mundo del revés, sí, donde la avaricia y la codicia no tiene fin.

Me preocupa y, sobre todo, me decepciona la actitud pasiva y la tibieza con la que estamos afrontando esta situación. Obviamente, que hay excepciones y dirigentes que levantaron voces en contra. Alfredo Cornejo, presidente de la UCR, manifestó que “el recorte a los jubilados desenmascara a Alberto Fernández: en campaña prometieron un aumento del 20% y en el gobierno prácticamente congelaron las jubilaciones”. Mario Negri, presidente del interbloque Juntos por el Cambio en la Cámara Baja, aseguró que la fórmula de movilidad jubilatoria del Gobierno “ignora la inflación” y afirmó que eso es “tener una miopía” respecto a la evolución de los precios en Argentina.

Aunque parezca mentira o ciencia ficción, en el mismo día, el juez federal de la Seguridad Social, Ezequiel Pérez Nami, dictaminó que la actual Vicepresidenta cobre dos jubilaciones de privilegio, un doble beneficio previsional (su asignación vitalicia que le corresponde) pero también, la pensión por el fallecimiento de Néstor Kirchner. La suma total, según especialistas, le permitiría cobrar unos dos millones por mes y 100 millones de pesos más, como intereses retroactivos (sin pagar impuesto a las ganancias) Es decir, unas 100 veces más la jubilación mínima ($19.035), monto que según el defensor de la Tercera Edad, Eugenio Semino, cobran alrededor de 4,5 millones de jubilados en la actualidad.

Lo mencionado anteriormente es totalmente incompatible según la ley 24.018. Dicha norma establece, “en su artículo 5, que el cobro de la asignación de expresidente es "incompatible con el goce de toda jubilación, pensión, retiro o prestación graciable nacional, provincial o municipal, sin perjuicio del derecho de los interesados a optar" en el supuesto de contar ya con uno de esos ingresos.

Las únicas personas que poseen facultad para apelar este fallo son Fernanda Raverta, titular de Anses y dirigente de la Cámpora, y el procurador del Tesoro, Carlos Zannini. Todo dicho, esperanzas de revertir este acto inmoral y antiético, no hay.

El peronismo, “férreo defensor de la justicia social y de la igualdad”, no opinó nada al respecto. Silencio cómplice en todos los sectores identificados con este partido. Lo mismo no sucedió cuando rechazaron bajo una lluvia de piedras la ley 27.426 (reforma previsional propuesta por Juntos por el Cambio). Es más, debo decir que existen estudios económicos que afirman que el jubilado hubiera obtenido mayores haberes, en el 2020, si la ley mencionada siguiera vigente.

Debemos garantizar una calidad de vida digna a nuestros jubilados, personas que toda su vida trabajaron y aportaron. Es una deuda histórica que el Estado tiene con ellos. Ojalá deje de ser un anhelo para transformarse en una acción concreta y real.

Por **Martin Tognola**.

_Profesor de Educación Superior en Ciencias de la Educación._