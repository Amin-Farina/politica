+++
author = "Alfredo Cornejo"
author2 = "Patricia Bullrich"
date = 2021-10-21T03:00:00Z
description = ""
image = "/images/photo5157165119086635495.jpg"
image_webp = "/images/photo5157165119086635495.jpg"
title = "El sur argentino está en peligro por el abandono presidencial"

+++
El Presidente de la Nación peca de ignorante eludiendo sus responsabilidades en materia de seguridad. El sur argentino está en peligro por el abandono presidencial.

Río Negro, 21 de octubre de 2021

Sr. Presidente de la Nación Alberto Fernández

De su urgente consideración:

Nos encontramos en este momento recorriendo las provincias de Neuquén y Río Negro. Nuestros representantes en esta región, Aníbal Tortoriello y Pablo Cervi, nos describieron el nivel de angustia y miedo que atraviesa la población.

Le expresamos de manera fehaciente nuestra preocupación por el desconocimiento y la negligencia manifiesta que usted expresa sobre los mandatos constitucionales referidos a la aplicación del Art. 13 de la Ley de Seguridad Interior, que faculta la constitución de un Comité de Crisis en el ámbito del Consejo de Seguridad Interior, cuya misión será ejercer la conducción política y supervisión operacional de los cuerpos policiales y fuerzas de seguridad federales y provinciales que se encuentren empeñados en el reestablecimiento de la seguridad interior en cualquier lugar del territorio nacional.

Preocupa, al límite, que las autoridades nacionales dejen a la intemperie a las autoridades provinciales elegidas democráticamente. Esto quedó plasmado en la insólita respuesta de ayer, 20 octubre, que usted mandó a la gobernadora de Río Negro, Arabela Carreras, manifestando que es “casi un favor” el que hace el Poder Ejecutivo Nacional al enviarle efectivos de Gendarmería. Como usted dijo y quedó expresado de manera textual, “no es función del Gobierno Nacional reforzar la seguridad en la región”.

Usted dejó reflejada una decisión política de abandono hacia la provincia de Río Negro y toda la región patagónica, mientras la Cancillería Argentina acude prestamente a intentar dar protección a un delincuente que niega la existencia de nuestro país, habiendo sido condenado en la República de Chile.

Nos solidarizamos y nos ponemos a disposición de la gobernadora Carreras y de los demás gobernadores de la región. Al tiempo, alertamos sobre las peligrosísimas consecuencias que esta señal irresponsable de abandono puede desencadenar para los intereses nacionales.

Exigimos la inmediata rectificación de su posición y que instruya de forma urgente al Ministro de Seguridad de la Nación, para que garantice la solicitud de la gobernadora Carreras y los futuros pedidos de los gobiernos patagónicos afectados por el flagelo de la violencia.

**Alfredo Cornejo -  Patricia Bullrich**  
Presidente UCR    -  Presidenta PRO