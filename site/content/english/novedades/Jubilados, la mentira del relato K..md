+++
author = "Ramiro Escalante"
author2 = "Facundo Goyeneche"
date = 2020-11-22T22:00:00Z
description = ""
image = "/images/placa-n5.jpg"
image_webp = "/images/placa-n5.jpg"
title = "Jubilados, la mentira del relato K"

+++
**Jubilados, la mentira del relato K.**

Alberto Fernández propone una fórmula para ajustar las jubilaciones en función de la variación de los salarios y la recaudación de impuestos que van al Anses, pero lo más llamativo es que esta fórmula NO CONTEMPLA A LA INFLACIÓN.

Esta postura del gobierno representa una vez más una mentira de la gestión populista del actual presidente dejando muy atrás sus promesas electorales y su opinión respecto los jubilados.

Con esta fórmula los jubilados pierden poder adquisitivo sumado a todo lo que ya perdieron desde que este gobierno decidió arbitrariamente suspender la movilidad jubilatoria establecida en la ley N°27.426 denominada como “Reforma Previsional”, que modificó la Ley N°24.241 y la Ley N°26.417, referidas al Sistema Integrado de Jubilaciones y Pensiones y a la movilidad de las Prestaciones Previsionales, respectivamente.

Muy atrás quedo aquellos eufóricos discursos kirchnerista del año 2017 y la violenta puesta en escena con las movilizaciones al congreso de la nación cuando se trataba la ley de movilidad jubilatoria propuesta por Juntos por el Cambio, el escenario muto y al parecer los jubilados van a ser la variable de ajuste para equilibrar la ya desequilibra finanzas publica del gobierno de Alberto Fernández.

El cálculo es simple y la matemática (por suerte) es objetiva, la jubilación mínima en argentina es de $18.128,85 pesos, luego del aumento que informó el pasado agosto la [Administración Nacional de Seguridad Social (ANSES)](https://www.anses.gob.ar/) del 7,5% cuando debería haber sido de 9,88%. Las jubilaciones y pensiones tendrán un aumento del 5% en diciembre, según anunciaron el jefe de Gabinete, Santiago Cafiero, y la titular de la Administración Nacional de la Seguridad Social (Anses), Fernanda Raverta, con este incremento, las jubilaciones mínimas aumentarán un 35,3% en lo que va del año en tanto, si se hubiese aplicado la fórmula de movilidad suspendida, los aumentos hubiesen sido de 42% para todo 2020.

Otro dato no menos llamativo es que para el Presupuesto 2021 se contempla un aumento de la Seguridad Social del 31%, varios organismos ya están advirtiendo que la inflación superará ampliamente ese número. Según el Instituto Argentino de Análisis Fiscal del Estado, con la suspensión de la movilidad jubilatoria en 2020 el Estado ajusto por 72.000 mil millones. Estos aumentos arbitrario sin reglas claras fue anunciado por el gobierno como una medida que le iba a permitir a los jubilados ganarle a la inflación, sin embargo debido a la crisis que vivimos esto forma más bien parte de un relato muy diferente a la realidad que viven más de 8 millones de adultos mayores en nuestro país, de este número la mitad percibe una jubilación mínima, ¿se entiende ahora por que decimos que con este gobierno los jubilados van a tener que esperar?

Cuando un estado tiene como bandera propia a los jubilados toma medidas que realmente puede cambiarles su calidad de vida para bien, esto es lo que comenzó a hacer la gestión de Cambiemos al lanzar el programa de Reparación Histórica donde se buscó celebrar acuerdos para el pago de sentencias de juicios previsionales, dando prioridad a los mayores de 80 años y a los que tienen enfermedades graves, también se dejó de apelar las sentencias judiciales. Se estableció que todas las ganancias del fondo de garantía de sustentabilidad se utilicen para pagar los juicios, es menester señalar esto ya que muy a menudo los fondos de la seguridad social fueron una impresionante caja de la que se sirvieron discrecionalmente los políticos para hacer frente al déficit fiscal derivado de la irresponsabilidad y de la corrupción del sector público. Durante el gobierno de Cristina Fernández de Kirchner se hizo una práctica habitual de ello.

De esta manera durante la gestión de Macri con la RH beneficio a más de 740.000 jubilados nacionales. A esta medida también es importante sumar la PUAM (pensión universal al adulto mayor) que vino a garantizar un ingreso universal a los adultos mayores que no tuvieron aportes, sin tener que depender del humor de los gobernantes de turno.

En síntesis, nuevamente, los beneficiarios del sistema jubilatorio van a recibir ajustes inferiores a la inflación, de modo tal que su capacidad de ingreso vuelve a sufrir un nuevo retroceso, para el gobierno de Alberto Fernández los jubilados son una parte más del relato populista.

**Dr. Ramiro Escalante (ex Gerente ANSES UDAI Corrientes)**

**Colaboración: Dr. Facundo Goyeneche (Asesor legislativo, Dip. Nac. E. Regidor).**