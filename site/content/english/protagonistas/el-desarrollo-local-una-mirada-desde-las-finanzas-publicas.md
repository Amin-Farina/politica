+++
author = "Santiago Merino"
author2 = ""
date = 2020-12-13T16:00:00Z
description = ""
image = "/images/placa-p3-santi.jpg"
image_webp = "/images/placa-p3-santi.jpg"
title = "El Desarrollo Local: una mirada desde las Finanzas Públicas"

+++
# "El Desarrollo Local: una mirada desde las Finanzas Públicas"

## **Crecimiento y Desarrollo. Un enfoque integral.**

_Por Santiago Merino._

Muchas veces, se suele priorizar el crecimiento en términos económicos sin una mirada integral, perdiendo de vista que el crecimiento es una condición para lograr el desarrollo, pero no es la única variable que debemos considerar.

Claro está, que si un país, una provincia, o una ciudad no tiene un crecimiento sostenido en términos económicos, jamás podrá alcanzar el pleno desarrollo, pero también existen una gran cantidad de casos, sobre todo en nuestro país, en los cuales este crecimiento no se plasmó en un desarrollo integral.

_¿Por qué algunas sociedades se desarrollan y otras no?_ Con este interrogante, Fabio Quetglas (Diputado Nacional UCR y Mg. en Gestión de Ciudades) nos pone a reflexionar a los miembros de los gobiernos, a las organizaciones empresariales y a la sociedad civil, sobre qué políticas públicas y que acciones podemos llevar adelante para lograr el desarrollo.

Debemos pensar al desarrollo local desde un enfoque integral que nos permita combinar los distintos factores sociales, institucionales, territoriales y económicos, que promuevan consensos básicos que nos guíen al cumplimiento de los objetivos compartidos que como sociedad nos proponemos alcanzar. Además de la integralidad, debemos considerar que los motores que impulsen el crecimiento, y consecuentemente el desarrollo sean aquellos que la provincia o la ciudad reconozcan como propios, por ejemplo, no pensar al desarrollo en los términos de instalar grandes industrias para generar crecimiento, sino impulsarlo con actividades que culturalmente sean reconocidas por la sociedad.

En este sentido, las instituciones ocupan un rol central, ya que una mejor calidad de estas nos permitirá resolver conflictos sociales y satisfacer las necesidades públicas de una manera más eficiente, para lograr una mejor distribución del ingreso, generar empleo y por supuesto, lograr un crecimiento sostenido que nos lleve al desarrollo territorial de nuestras ciudades.

Generar desarrollo local tiene un costo, e implica asignar los recursos del Estado para satisfacer las necesidades de la sociedad, que, a través del pago de los diferentes tributos, exige bienes y servicios públicos de calidad.

Lo mencionado en el párrafo precedente será la “brújula” que nos marque el camino en estas reflexiones, ya que, para cumplir sus objetivos, los gobiernos locales deben tener una estructura de ingresos y de gastos sustentable, planificada y con una mirada social, generando inclusión y participación ciudadana.

## **La importancia del Presupuesto y la participación ciudadana.**

La estructura de ingresos y gastos de un gobierno local se ve plasmada en la “Ordenanza de Presupuesto Anual” sancionada por el Honorable Concejo Deliberante.

El presupuesto siempre constituye el instrumento de asignación de recursos públicos en las instancias decisionales, pero también se convirtió en una herramienta importante para el control de la gestión, ya que los nuevos paradigmas políticos expresan la creciente demanda de la participación de la sociedad en los distintos asuntos del Estado.

Tradicionalmente, el presupuesto tiene un enfoque “erogativo”, donde el énfasis está puesto en lo que los gobiernos gastan, se centra en la legalidad de los gastos sin tener en cuenta el factor primario del proceso de asignación de recursos: las necesidades públicas. Esta modalidad ha sido la más utilizada a lo largo del tiempo por las distintas administraciones y de hecho sigue siendo utilizada actualmente.

Los principios de un nuevo modelo estatal, que como mencioné anteriormente, tiende hacia un aumento de la participación ciudadana, nos va guiando hacia una nueva forma de abordar el presupuesto público en los gobiernos locales. Debemos lograr una concepción de gestión integral que tiene como objetivo maximizar la satisfacción de las necesidades públicas, sin perder de vista las restricciones financieras y legales.

En nuestro país, y en general en los distintos gobiernos locales, nos enfrentamos a un problema en la asignación de los recursos, y entre sus principales causas nos encontramos con un déficit de programación en los Programas Generales de Gobierno.

Una de las soluciones a este problema la encontramos bajo la ya mencionada concepción de gestión integral (a diferencia de la concepción tradicional con un enfoque de asignación de recursos teniendo en cuenta solamente los gastos a realizar) que tiene como punto de partida a las necesidades públicas, y en este momento es donde el conocimiento del territorio y de los distintos actores sociales toman un rol protagónico.

Conocer el territorio nos permitirá conocer las carencias de la población, que darán origen a los bienes y/o servicios públicos que se deberán proveer para satisfacer las mismas. No es racional asignar recursos sin conocer las distintas necesidades y cuáles son las causas que las originan, ya que de acuerdo con las causas pueden surgir las distintas acciones necesarias para darles solución e influirán en los recursos que se asignen y quién será el responsable de la ejecución de estos.

## **Conclusiones y desafíos a futuro.**

Debemos tener instituciones de calidad y una mirada de gestión integral que combine los distintos factores para generar políticas públicas que nos ayuden a lograr el completo desarrollo local de nuestras ciudades, impulsando actividades que la sociedad reconoce como propias para poder involucrarse y formar parte de este camino.

Los gobiernos locales tienen que acelerar la transición de la concepción tradicional del presupuesto hacia la concepción integral para alcanzar el máximo objetivo de las políticas públicas que es satisfacer las necesidades de la población.

Al ser los recursos escasos y las necesidades ilimitadas, configurar el Programa General de Gobierno partiendo desde el conocimiento del territorio, la sociedad y sus necesidades insatisfechas y las causas que las producen nos permitirá establecer prioridades y tomar decisiones eficientes a la hora de asignar los recursos del Estado, ya que asignar sin conocer, nos mantendría en la misma posición de cara a los problemas que nos enfrentamos para generar el tan deseado desarrollo.

En Corrientes ya empezamos a transitar este camino, está en nosotros seguir perfeccionando la gestión y transformando la realidad mirando hacia el futuro, para conseguir la ciudad moderna y de oportunidades que nos merecemos.

**_Santiago Merino._** _Contador Público. Gerente General Fideicomiso de Administración Santa Catalina. Pro-Tesorero Comité Capital de la Juventud Radical._