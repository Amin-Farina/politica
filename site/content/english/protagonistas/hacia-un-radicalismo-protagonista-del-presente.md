+++
author = "Lautaro Barbis"
author2 = ""
date = 2020-12-29T03:00:00Z
description = ""
image = "/images/placa-p4-lautaro.jpg"
image_webp = "/images/placa-p4-lautaro.jpg"
title = "Hacia un radicalismo protagonista del presente"

+++

**Tenemos el enorme desafío de escuchar las ideas y comprender las distintas realidades que atraviesan los vecinos y las vecinas para transformar juntos nuestra realidad.**

**Por Lautaro Barbis (@lautarobarbis)**

¿Cuántas veces te dijeron que los jóvenes somos el futuro? Nosotros somos el presente. En 2021, quienes tenemos menos de 25 años vamos a ser 1 de cada 4 ciudadanos habilitados para votar. Somos una generación que nacimos y nos criamos en un mundo revolucionado. El volumen y la velocidad de la información que hoy tenemos al alcance de nuestras manos –en nuestros celulares- es inédito. Según un informe de la encuestadora Ipsos, el 97% de los jóvenes tenemos acceso a un smartphone y el 94% se informa a través de las redes sociales. Podemos conectarnos con nuestros vecinos, con otros argentinos o con alguien al otro lado del mundo, en menos tiempo de lo que se marcaba un número telefónico hace 20 años.

Crecimos al calor de la consagración de nuevos derechos sociales. Importantes leyes como Ley de Matrimonio Igualitario, la Ley de Identidad de Género y la Ley de Paridad de Género se materializaron a partir del incansable trabajo de colectividades, organizaciones de la sociedad civil, autoconvocados y el trabajo de muchos militantes políticos. Aprendimos a reclamar y poner en la agenda nuestros derechos como lo hacemos en las marchas contra la violencia de género #NiUnaMenos y debatiendo las formas y el fondo de la Interrupción Voluntaria del Embarazo (IVE). Para nuestra generación todo –absolutamente todo- está en discusión, lo importante es dar el debate, compartir ideas, visiones y necesidades.

Cada uno de nosotros y nosotras vive en circunstancias específicas, y en ellas desarrollamos particulares puntos de vista. Solo escuchándonos, conociéndonos y encontrándonos vamos a poder enriquecer, con cada una de esas miradas, la manera en la que intervenimos políticamente.

Somos la generación que percibe la política como la mejor herramienta para mejorar la vida de nuestra comunidad. Entendemos la importancia de las ideas, el diálogo y el debate, en el marco del respeto democrático, como mecanismo para canalizar soluciones. Mejorar nuestro mundo no solo es posible, es necesario y urgente.

**¡Adelante radicales!**

Es evidente que cada generación tiene una historia, valores e identidad que la define. Pero también tiene desafíos que la ponen a prueba.

Como jóvenes, nuestro desafío es mejorar nuestro entorno. Nos preocupa y moviliza la pobreza, la desigualdad, la discriminación, pero también la deforestación, la contaminación, los incendios descontrolados y toda irresponsabilidad con la naturaleza.

Como radicales tenemos un desafío renovado: fortalecer la representación política, vitalizar la participación de los jóvenes y mejorar la calidad de nuestra democracia. Esto solo será posible si comenzamos por un diagnóstico certero de la realidad que atravesamos. Tenemos que ser capaces de conocer las frustraciones, los temores, los sueños y las ilusiones que tiene cada uno de nuestros vecinos y vecinas para ser más eficaces. Pero también necesitamos incorporar más jóvenes, ideas frescas, diversidad y nuevos enfoques a este, nuestro querido partido centenario.

Para convencer, debemos estar convencidos. Nosotros estamos convencidos de que somos parte de un enorme equipo de jóvenes con un gran potencial para llevar adelante transformaciones que trasciendan nuestra propia generación. Debemos dejar de lado la insistente pregunta del “¿Por qué somos radicales?” y comenzar a proyectar en equipo “¿Para qué somos radicales?”. Esta es la clave: encontrar un horizonte compartido que nos marque el camino en común para recorrerlo juntos.

En este proceso de reconfiguración de nuestra representación política joven, los y las militantes asumimos un rol protagónico. Está en nosotros la profunda vocación por escuchar las ideas y comprender las realidades que atraviesan nuestros vecinos y vecinas para arribar a soluciones superadoras capaces de incidir positivamente en nuestro entorno. Tenemos que formar a cada militante, brindándole las herramientas necesarias para desplegar al máximo su potencialidad y, así, construir juntos una usina generadora de cuadros políticos de excelencia.

Las transformaciones nacionales y provinciales comienzan a nivel local. Es en cada pueblo y cada ciudad donde tenemos el enorme desafío de construir juntos los cimientos para un radicalismo protagonista del presente: un partido político moderno, predispuesto a convocar a los vecinos y las vecinas para trabajar en equipo, codo a codo, en la búsqueda de las mejores soluciones a sus problemas.

Somos quienes no nos quedamos de brazos cruzados, quienes decidimos participar, involucrarnos y comprometernos con nuestra realidad para transformarla juntos.

**Ahora, más unidos que nunca: ¡_Adelante Radicales_!**

_Lautaro Barbis. Paso de la Patria.   
 Estudiante de Ciencia Política. Militante de la Unión Cívica Radical._