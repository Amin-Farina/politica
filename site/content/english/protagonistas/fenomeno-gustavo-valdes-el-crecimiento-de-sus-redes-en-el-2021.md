+++
author = "Pilar Fernández"
author2 = "Francisco Bosco"
date = 2021-10-24T11:00:00Z
description = ""
image = "/images/24-10-protagonistas.jpg"
image_webp = "/images/24-10-protagonistas.jpg"
title = "Fenómeno Gustavo Valdés: el crecimiento de sus redes en el 2021"

+++
Durante los meses de agosto y septiembre la campaña política fue protagonista no solamente en la provincia, sino a nivel país. Sin dudas estamos ante un año electoral atípico, no solamente debido al escenario de pandemia que todavía se está atravesando, sino también por los resultados que dejaron a nivel local, como a nivel nacional.

En la provincia de Corrientes tuvimos un agosto muy movido políticamente con las elecciones a gobernador, intendentes de varias localidades, concejales, diputados y senadores provinciales. A nivel nacional, el mes pasado se disputaron las PASO en las que la sociedad expresó quiénes serían los candidatos a participar en la conformación del Congreso de la Nación.

En Corrientes se vivió una elección histórica por donde se la mire. El actual gobernador, Gustavo Valdés, fue reelecto con el 76,9% de los votos, llegando a alcanzar en varias localidades más del 90%.

Estos resultados del gobernador representaron una bocanada de aire de renovación dentro del partido radical.

Semanas después, Facundo Manes, ganaba las PASO representando a los radicales dentro del espacio de Cambiemos, donde también compite el candidato del PRO, Diego Santilli.

La reelección de Valdés consolida un escenario en el que el radicalismo toma mayor fuerza en la discusión por el poder político.

Todo comenzó este año con las elecciones legislativas en Jujuy que dieron por ganadora a la fuerza que lidera el gobernador radical, Gerardo Morales.

Estas contiendas electorales pusieron en el foco a los gobernadores radicales y el análisis de cómo llegaron a esos resultados en sus provincias.

En este articulo intentaremos sumar a esta cuestión analitica, haciendo énfasis en como se desempeñaron, a través de la toma de indicadores y datos reales de sus redes sociales los 3 gobernadores radicales: Gustavo Valdés (Corrientes), Gerardo Morales (Jujuy) y Rodolfo Suárez (Mendoza).

**La comunicación como herramienta para crear cercanía**

Estamos sumergidos en la era digital en la que el boom de las redes sociales, vino a imponer una lógica propia de organización y creación de contenidos, con la posibilidad de hablarle a un público mucho más amplio.

Pero asimismo con la excepcional ventaja de hacerlo de manera segmentada, haciéndole llegar a un público específico ya sea por edad, lugar donde vive o por las preferencias que tiene, el mensaje ideal para este segmento, comunicando así una idea en particular.

Los gobernadores han sabido aprovechar estas herramientas desarrollando contenidos en sus redes sociales personales. En el caso de Valdés en Corrientes, el crecimiento de sus redes se dispararon durante su primera campaña a gobernador en 2017 y no paró desde entonces.

Actualmente en Facebook tiene más de 191 mil seguidores, 50 mil en Twitter y 97 mil en Instagram.

De los 3 gobernadores radicales, Valdés cuenta con más seguidores en Facebook e Instagram por sobre Morales y Suárez.

Esto requiere profesionalizar el área de la comunicación y los equipos que se dedican a estas tareas dentro de los espacios de gobierno y política.

Las redes sociales se mantienen en constante cambio. Lo sucedido con Cambridge Analytica con la venta de datos personales de usuarios, o la campaña de Donald Trump, en la que las fake news eran moneda corriente, fueron escándalos que hicieron dudar de la seguridad y transparencia de Facebook.

Frente a esto, Facebook fue cambiando sus políticas de privacidad y cuidando al usuario de mecanismos de engaño en campaña política.

Es por ello que los equipos de comunicación deben estar cada vez más actualizados con los cambios que se producen, los nuevos lenguajes que se utilizan y las aplicaciones que van surgiendo y que requieren también su entendimiento para aplicarlas en política, como por ejemplo TikTok.

Las redes sociales se convirtieron en un camino de doble vía, en las que se comunican mensajes pero estos son respondidos por el público, audiencia o votantes. Inclusive muchas veces los contenidos que se crean responden a consultas y demandas de la población ante cierta cuestión. Las personas en cada una de las publicaciones dejan reacciones, comentarios y compartidos.

Todas estas acciones generan el principal indicador que se tiene en Redes Sociales: el “Engagement”. Este indicador cuantitativo nos permite conocer el grado de participación de los protagonistas y realizar estudios estadísticos cuantitativos y cualitativos, utilizando la ciencia de datos

Para el análisis se tomó el periodo entre los meses de enero y septiembre del 2021 en las redes sociales Facebook e Instagram. Se utilizaron herramientas de captura de datos en las redes sociales citadas y herramientas de inteligencia de negocio para los gráficos.

**ANALISIS EN FACEBOOK**

**Me gustas**

El número de «Me gusta» también es llamado la cantidad de «Fans» y se ha asociado siempre con el tamaño de la comunidad de una marca en Facebook. Analizaremos la cantidad de fans que tienen cada uno a la fecha de hoy 30 de septiembre del 2021.

![](/images/gobers.jpg)

Analizando el crecimiento del 30 de septiembre con respecto al 1 de enero del 2021 cada Fan Page creció nominalmente de la siguiente manera:

![](/images/gobers-2.jpg)

El crecimiento en cuestión de seguidores en redes sociales podría tener relación con el escenario de pandemia que venimos atravesando hace ya casi 2 años.

Respecto a la desinformación e incertidumbre frente a muchas cuestiones de la pandemia y como fue impactando en todos los aspectos de nuestra vida, los ciudadanos precisaron una fuente de información fidedigna y confiable.

Debido a eso el interés por las publicaciones del gobierno dieron un salto cuantitativo importante, ya que de esta manera el público podía enterarse de las políticas llevadas adelante para hacerle frente al coronavirus.

En esa línea muchas publicaciones de los 3 gobernadores en el último tiempo tuvieron que ver con la pandemia. El gobernador Valdés asimismo entendió la dinámica y necesidades del público de mayor información.

Es por ello que hasta incrementó las conferencias de prensa transmitidas en vivo por todas las redes sociales oficiales, las cuales tuvieron alta participación y visualización de la ciudadanía.

![](/images/gobers-3.jpg)

_Gráfico 1: Evolución acumulada mensual del 2021 de los 3 Gobernadores radicales, enero a septiembre de “Me Gusta” en la red social Facebook_

![](/images/gobers-4.jpg)

Gráfico 2: Evolución del indicador engagement de los 3 gobernadores Radicales

Aquí también se puede ver al gobernador de Corrientes predominando la evolución desde el mes de enero a septiembre de 2021. Un dato no menor es como el engagement se representa en interacciones por cada publicación:

![](/images/gobers-5.jpg)

Es decir por cada publicación Gustavo Valdés obtiene 1853 interacciones entre me gusta, comentarios y compartidos en promedio.

**ANÁLISIS EN INSTAGRAM**

Dentro de Instagram, los gobernadores también generan mucho contenido para sus seguidores y para atraer nuevos. En este caso, analizaremos la cantidad de fans que tienen cada uno a la fecha del 21 de septiembre del 2021.

![](/images/gobers-6.jpg)

Si tomamos como período del 1 de enero 2021 al 30 de septiembre de este mismo año, vamos a poder establecer cuántos seguidores crecieron los candidatos durante ese tiempo.

![](/images/gobers-7.jpg)

En esta plataforma social también se lo puede ver a Gustavo Valdés en el primer puesto, tanto de cantidad de seguidores, como en crecimiento durante los últimos meses.

![](/images/gobers-8.jpg)

_Gráfico 4: Evolución de los 3 Gobernadores radicales, enero a septiembre 2021 respecto a sus seguidores en Instagram_

![](/images/gobers-9.jpg)

Gráfico 5: Evolución del indicador engagement de los 3 gobernadores Radicales

También en esta red se observa un predominio del Gobernador Valdés de Corrientes. Analizando el promedio engagement por publicación obtenemos la siguiente tabla

![](/images/gobers-10.jpg)

Es decir por cada publicación Gustavo Valdés obtiene en Instagram 1809 interacciones (engagement) en promedio.

En todos los casos analizados de Facebook e Instagram se observa el crecimiento exponencial que han tenido las redes de Gustavo Valdés.

Los números de sus redes sociales van en constante crecimiento junto a sus habilidades de comunicar su gestión de forma inteligente. Lo mismo que sucede con los gobernadores Morales y Suárez pero con un crecimiento más lento.

Esto sumado a la imagen positiva sostenida que tuvieron durante todo el año, a pesar de la pandemia, fueron algunos de los factores que suman en el entramado de cuestiones y acciones por las que lograron el éxito en sus respectivas provincias.

Las actividades de los gobernadores radicales ocupan fuertemente la agenda nacional y son observados por toda la esfera política, siendo el resultado de una construcción que viene desde hace tiempo, donde la comunicación en redes sociales y sus alcances resulta primordial.

* _Pilar Fernández, Licenciada en Comunicación y periodista especializada en Comunicación Política._
* _Francisco Bosco, Licenciado en Sistemas y Director de Estadísticas y Censos de la Provincia de Corrientes._