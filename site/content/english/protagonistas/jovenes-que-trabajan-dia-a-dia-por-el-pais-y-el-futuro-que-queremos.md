+++
author = "Valeria Pavon"
author2 = ""
date = 2022-08-16T11:00:00Z
description = ""
image = "/images/img_20220816_125402_496.jpg"
image_webp = "/images/img_20220816_125402_496.jpg"
title = "Jóvenes que trabajan día a día por el país y el futuro que queremos"

+++
La **histórica Juventud Radical** ha sido y es uno de los **movimientos de jóvenes más grandes del país** y que, a pesar de los años, **se consolida a lo largo ya lo ancho del territorio argentino.**

Pero hablemos un poco de **cómo está conformada al día de hoy** , **qué rol cumple en la sociedad** y la **fuerza que ha adquirido en la toma de decisiones partidarias y políticas.**

**A nivel nacional** , la **mesa directiva** está conformada por **un grupo de jóvenes** que llevamos adelante **políticas públicas y de acción social** , recorriendo cada rincón del país, consolidando el partido y afianzando alianzas estratégicas para que los y las integrantes de la agrupación mantengan una formación constante .

El **objetivo principal de la Juventud Radical** e**es que nadie se quede atras** . Hace años, era pensador hablar de **paridad de género** , de **inclusión** y de **derechos de la comunidad LGBTIQ+** o, simplemente, era visto como algo raro que **una mujer ocupe un lugar o carga donde se toman decisiones importantes** que, posteriormente, inciden en la vida de la población argentina.

#### Nadie se queda atrás

**Educar, debatir y dialogar** , ese es **el camino que queremos marcar.** Desde el propio partido **fomentamos encuentros provinciales, regionales y nacionales** para mantener una **capacitación constante y debatir** temas actuales que azotan al país.

**La juventud** que hoy sale a la calle y exige **oportunidades laborales y mejores condiciones de vida** es la misma que se involucra y se forma para liderar el día de mañana.

Estos **momentos son cruciales para definir cómo seguirá el rumbo de nuestro** y evitar que más jóvenes prefieran irse por miedo a la inseguridad social y económica. Se acercan las elecciones presidenciales 2023 y desde la JR mantenemos vivo al partido, **comprometiéndose con la causa y decidiendo cuál es el futuro que queremos.**

_Valeria Pavón_

_Presidente Juventud Radical de la Argentina._