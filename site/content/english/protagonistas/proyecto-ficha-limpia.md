+++
author = "Tiago Puente"
author2 = ""
date = 2022-08-26T11:00:00Z
description = ""
image = "/images/placa-p15-tiago.jpg"
image_webp = "/images/placa-p15-tiago.jpg"
title = "Proyecto \"Ficha Limpia\""

+++
Conocí el movimiento ciudadano de Ficha Limpia en diciembre del 2019, a pocos días de jurar como Diputado Provincial, y, desde ese día, la iniciativa formó parte de mi agenda legislativa, porque las y los catamarqueños no podíamos quedar al margen de este movimiento, porque al igual que el resto de las provincias y el país, nos merecemos una mejor calidad democrática y un Estado sin delincuentes.

Es así, que en el año 2020, con mi equipo presentamos un proyecto de ficha limpia -elaborado siguiendo el modelo de Mendoza- que desde el inicio se topó con obstáculos: no fue tratado ni en la comisión, ni en el recinto, lo catalogaron de “inconstitucional” por –según sus criterios- violentar la presunción de inocencia (para ponerlos en contexto, de los 41 diputados, solo 15 pertenecemos a Juntos por el Cambio).

Este año, insistimos en un nuevo proyecto de ficha limpia, que corrió la misma suerte. Es así, que decidimos iniciar la campaña en las calles, con el objetivo de que quienes se oponían, entiendan que ficha limpia no era un capricho de Tiago Puente, sino una demanda de la sociedad. Finalmente, el clamor y pedido de la ciudadanía, logró que Ficha Limpia doblegue la resistencia del oficialismo, quienes presentaron un proyecto con otra denominación, pero que respondía a la esencia y finalidad de aquél. El camino continuó en la comisión, donde lamentablemente, evitaron el tratamiento conjunto de ambas iniciativas, negándole a la sociedad la elaboración de una propuesta consensuada y superadora.

A pesar de esto, no pusimos palos en la rueda, porque entendimos que esta iniciativa está por encima de cualquier nombre o partido, y es por esa razón, que celebramos y acompañamos el proyecto del Bloque del Frente de Todos, y así logramos que el pasado miércoles 13 de Julio, la Cámara de Diputados de Catamarca, en forma unánime, diera media sanción a Ficha Limpia.

Como legisladores, demostramos que cuando hay iniciativas que hacen al fortalecimiento de nuestra democracia, las diferencias partidarias pueden y deben ser superadas. Que es importante tener la madurez, el compromiso y la responsabilidad del consenso y el diálogo, no sólo en este tema, sino en todo aquél que sea para mejorar la vida de las personas.

Ahora resta la aprobación en el Senado y la promulgación del Gobernador para que Ficha Limpia se convierta en Ley. Confío en que así será, porque como lo dije desde un principio, éste no es un proyecto contra nadie, sino a favor de las instituciones y la República.

Orgullosamente, podemos decir que Ficha Limpia llegó a Catamarca para quedarse, y esperamos que lo mismo suceda en el resto de las provincias y el país, porque los tiempos que atravesamos como Estado, requieren de más transparencia, honestidad y compromiso al servicio del bien común.

No quiero despedirme sin agradecer y resaltar la labor del movimiento ciudadano que le dió origen, a Gastón Marra, Fanny Mandelbaum y otros, que fueron los artífices principales de que Ficha Limpia tome estado en la agenda pública.

Tiago Puente

Diputado Provincial de Catamarca.-