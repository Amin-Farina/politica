+++
author = "Ubaldo Leiva"
author2 = ""
date = 2021-08-10T14:00:00Z
description = ""
image = "/images/photo4941010801037257106.jpg"
image_webp = "/images/photo4941010801037257106.jpg"
title = "Jóvenes en las listas - Perugorría"

+++
Ubaldo Leiva, es actual candidato a Vice Intendente de la localidad de Perugorría.

Seguimos conociendo a nuestros representantes jóvenes en las listas. ¡Adelante Radicales!

Mirá el video completo acá: [https://youtu.be/JHzWy46r0As](https://youtu.be/JHzWy46r0As "https://youtu.be/JHzWy46r0As")