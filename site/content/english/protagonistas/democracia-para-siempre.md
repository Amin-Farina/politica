+++
author = ""
author2 = ""
date = 2020-10-30T03:00:00Z
description = ""
image = "/images/efemerides-30-de-octubre.jpg"
image_webp = "/images/efemerides-30-de-octubre.jpg"
title = "Democracia para Siempre"

+++
[30-de-octubre.mp4](/images/30-de-octubre.mp4 "30-de-octubre.mp4") 

Te invitamos a mirar este video.

A 37 años de la recuperación de la Democracia, queremos recordar ese discurso de Raúl Alfonsín, que nos movilizaba a creer en la política, a defender las palabras del preámbulo y a militar por un país mejor. Al rendir este homenaje debemos sostener la bandera de este partido centenario y continuar la lucha: de la honestidad, austeridad, diálogo, consenso, compromiso militante, seriedad, y responsabilidad para enfrentar los problemas de la actualidad.