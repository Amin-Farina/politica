+++
author = "Valeria Pavón"
author2 = ""
date = 2021-07-26T11:00:00Z
description = ""
draft = true
image = ""
image_webp = ""
title = "Con los pies en el presente y la mirada en el futuro"

+++
Quienes comprendemos a la política como vocación de servicio sabemos que, en incontables oportunidades, nos vemos en la necesidad de postergar cuestiones personales porque la realidad a la cual nos enfrentamos francamente nos interpela despertando un filamento sensible que nos conmueve y nos compromete con la misma para brindar hasta el último de nuestros esfuerzos por transformarla. Desde chica, esta vocación me invadió y decidí involucrarme con la realidad que atraviesan diariamente los y las jóvenes de nuestra querida Provincia de Corrientes para escuchar sus distintas visiones, conocer sus sueños e invitarlos a trabajar en equipo para transformar su resignación en acción y, así, construir juntos nuestro futuro.

Esta invitación a construir nuestro futuro, no es más que preguntarnos colectivamente cómo nos gustaría vernos desde el presente hacia los próximos años. A simple vista, parecería ser una pregunta menor pero no lo es. Pues, el primer paso para saber hacia dónde queremos ir es conocer profundamente dónde estamos parados. Lo cierto es que, a lo largo de los últimos veinte años, nuestra querida Provincia de Corrientes ha experimentado muchas transformaciones, desde cuestiones tan elementales, como garantizar el pago en tiempo y forma de los salarios de los trabajadores estatales provinciales, hasta cuestiones trascendentales, como el establecimiento de parques industriales para generar valor agregado a nuestras economías regionales.

Estas transformaciones lejos están de considerarse fruto del mero paso del tiempo son, en todo caso, el resultado de una extensísima red de esfuerzos concadenados de actores políticos, económicos y sociales que coincidieron en empujar a la par una misma visión de nuestra provincia, a partir de los escombros en los cuales nos hallábamos. En parte por esto, hoy podemos estar planteándonos hacia dónde queremos ir con la seguridad de tener pilares sólidos sobre los cuales hacer pie.

El próximo 29 de agosto, una de cada cuatro personas que asistan a los centros de votación tendrán menos de treinta años. Considero que no podemos evitar dirigirnos a los mismos, sin mencionar siquiera las problemáticas sociales que les atraviesan. El universo joven, tiene la particularidad de verse totalmente entrecruzado por demandas de naturaleza heterogénea pero, me es ineludible preguntarme: ¿Qué tienen en común la problemática ambiental y la lucha por la igualdad de género? O, bien ¿Cuál es el punto de conexión entre el acceso al primer empleo y la enseñanza obligatoria de educación sexual integral? Probablemente, la repuesta nos conduzca nuevamente al inicio: la necesidad de pensar nuestro futuro colectivo.

El futuro es lo que nos convoca a todos porque entendemos que sin presente no hay futuro pero, tampoco existe el presente si no nos planteamos un horizonte en común hacia el cual caminar juntos. Acepté el desafío de ser candidata a Diputada Provincial porque considero que las demandas de los jóvenes de entre dieciocho y treinta años de nuestra querida Provincia de Corrientes, necesitan tener un oído al cual acudir para ser escuchados y, a la vez, una voz que nos represente genuinamente desde la esfera política. Quiero invitarlos a escucharnos, a compartir nuestras ideas y conocer nuestros sueños. En fin, quiero invitarlos a que construyamos juntos nuestro futuro.

Valeria Pavón.-