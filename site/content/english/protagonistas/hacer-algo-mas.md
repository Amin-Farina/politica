+++
author = "Mercedes Mestres"
author2 = ""
date = 2021-04-26T12:00:00Z
description = ""
image = "/images/placa-p7-mercee.jpg"
image_webp = "/images/placa-p7-mercee.jpg"
title = "Hacer algo más"

+++
_Dejemos de buscar culpables y empecemos a encontrar soluciones._

_“Los políticos no hacen nada”; “Este país es inviable”; “La política está podrida”_ , son frases conocidas que seguramente hemos escuchado, e incluso pronunciado, más de una vez. Si son ciertas o no dependerá de la interpretación de cada uno / a. Estoy segura que somos muchos / as quienes estamos 100% en desacuerdo con todas ellas.

Lo que propongo es que corramos un poco el foco de la mirada, que dejemos sólo de echarle la culpa a los gobernantes –entendiendo siempre que por su rol tiene mayor grado de responsabilidad- y empecemos a hacernos cargo de lo que nos corresponde. Por dos motivos: porque los / as gobernantes no están donde están por arte de magia y porque como ciudadanos podemos y debemos hacer algo más que sólo elegir –y luego desestimar- a nuestros / as representantes.

Pero, _¿por qué debemos hacer algo más?_ Porque una sociedad no la construyen sólo sus gobernantes sino también, y fundamentalmente, sus habitantes en las acciones de cada día. _¿Y para qué?_ Para generar un cambio positivo y empezar a construir el país justo, libre y próspero que soñamos. Y lo más importante: _¿cómo podemos hacerlo?_ Afortunadamente, de muchas maneras, diferentes pero todas necesarias.

Si aceptamos que la política es la mejor y mayor herramienta de transformación social y que, como lo establece nuestro sistema electoral, los partidos políticos son los canales habilitados para presentar y ganar una elección, entonces seguramente coincidiremos en que participar en la vida política-partidaria es una buena manera de empezar a hacernos cargos de lo que nos corresponde.

Hay quienes eligen hacerlo mediante acciones puntuales: en una escuela fiscalizando para una elección o en las calles participando de alguna manifestación. Recordemos aquellas mágicas marchas del #SíSePuede en 2019, que permitieron sumar 2,7 millones de votos más en las elecciones generales; o los banderazos del año pasado cuando el pueblo repudió las medidas del Gobierno Nacional, logrando que algunas dieran marcha atrás como el caso de Vicentín. Ver a hombres y mujeres, que quizás nunca habían salido a marchar pacíficamente, con sus banderas argentinas, cantando el himno nacional, es indescriptible. Hay algo invisible pero sumamente poderoso que se despierta y cobra vida en cada uno de esos encuentros masivos.

Si decidimos dar un paso más, podemos sumarnos como voluntarios/as a algún espacio político que coincida con nuestros valores e ideales. Participar activamente, generar proyectos, capacitarnos y prepararnos para ser los/as mejores en futuros cargos públicos. Ver a los/as jóvenes que deciden descruzar los brazos y sumarse a la juventud de mi partido para mejorar, con acciones diversas y concretas, la calidad de vida de los vecinos de su ciudad, me motiva a seguir trabajando para construir una política más humana, honesta y transparente cuyo objetivo máximo sea la búsqueda genuina del bien común.

Y también, desde el lugar que a cada uno/a le toque, es posible y necesario proponer ideas, además de tirar piedras. Quienes ocupamos cargos públicos somos representantes del pueblo y es nuestro deber, entre otros, recibir y escuchar a quienes nos eligieron (y a quienes no, también). Aceptar con humildad que no podemos saber de todo ni tener las soluciones a todos los problemas. Estoy convencida que la mejor -y quizás única- forma de cambiar nuestra porción del mundo es pensando y trabajando a la par.

_“Hay que hacer lo correcto, y algo más”,_ dijo una vez alguien en una charla. Una frase tan simple como reveladora. Ojalá podamos escucharla y pronunciarla más seguido. Y, sobre todo, llevarla a la práctica. ¿Se imaginan si todos/as decidiéramos “hacer algo más”?¿Se imaginan cuán diferente sería nuestra realidad? De sólo pensarlo me da piel de gallina y se me viene a la mente una sola palabra: ESPERANZA.

Mercedes Mestres

Concejal de la Ciudad de Corrientes.-