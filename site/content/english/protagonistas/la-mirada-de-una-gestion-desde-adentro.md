+++
author = "Nicolas Scofano"
author2 = ""
date = 2020-10-28T03:00:00Z
description = ""
image = "/images/placa-p2.jpg"
image_webp = "/images/placa-p2.jpg"
title = "La mirada de una gestión desde adentro"

+++
**En Primera Persona…**

**La mirada de una gestión desde adentro.**

Mi nombre es Nicolás Scófano, tengo 29 años, soy Trabajador Social.

Hace 3 años que formo parte del Equipo de Gestión del Intendente de la localidad de Bella Vista el Escribano Walter Chávez como Secretario de Desarrollo Humano.

Uno de los objetivos colectivos que nos motiva en la Juventud Radical es el saber ¿Para qué estamos preparados? ¿Podríamos asumir con responsabilidad espacios de toma de decisión? ¿Seremos capaces de sumarnos a lo que percibimos como una política de renovación?

Son cuestionamientos que nos mantienen alerta y en vilo, el Joven de hoy se encuentra en un proceso continuo de formación, busca estar actualizado a las nuevas demandas y saberes que plantea el mundo, el de la globalización, el de las redes sociales pero no deja de preocuparse por aquellos problemas estructurales que son una realidad, la pobreza, la violencia, las situaciones de vulnerabilidad.

**¿Cómo es la vida en gestión de los Jóvenes?**

En lo personal debo reconocer que cada inicio representa una turbulencia, muchas veces porque resulta salir de la zona de confort, sin embargo en este camino que vengo transitando es fundamental destacar el gran cambio de paradigmas que se puede ver en nuestros adultos, el acompañamiento hacia las nuevas generaciones, el reconocimiento y el hecho de abrir las puertas para ocupar lugares decisivos forma parte de este camino en el cual estamos inmersos.

Hay que ser consciente que no se llega solo, sino por el apoyo y sostén de un gran número de gente que como uno la viene militando desde hace mucho, creyendo en que podrán haber nuevas oportunidades, nuevos caminos, nuevos horizontes; pero tampoco se tiene individualmente la capacidad de sostenerse a lo largo del tiempo, sin el reconocimiento del equipo laboral que te rodea, de los afectos, pero por sobretodo de los ciudadanos de tu localidad, de tus vecinos, de aquellos que pueden ver en vos no un cargo o una función, sino que se toman el trabajo de mirar detrás de todo eso y encuentran una persona.

Allí está el secreto que también buscamos promover los jóvenes, la humildad, el altruismo, el hecho de que para entender a un vecino se tiene que ser y sentir como tal, esa actitud te permite cambiar realidades y en lo personal es lo que buscamos todos aquellos que nos animamos a desandar el camino de la política.

Entonces, ¿cómo no nos vamos a animar a cambiar la realidad los jóvenes si podemos sentirnos parte de la misma?, hoy ya no es una utopía o un pensamiento futurológico en el cual muchas de nuestras generaciones previas anhelaban poder vivir. Sé que aún nos falta mucho, dentro del partido, nos queda seguir siendo seres motivadores para que mas jóvenes se sumen a la causa de participar en la vida política, en la vida democrática, esa que sin dudas y sostengo dirige los hilos de nuestras vidas, debemos trabajar en el descreimiento del joven en la política, en su desinterés, debemos mostrarle la cara positiva aquella que busca el bien común, que busca luchar por los derechos que nos faltan, abrirles las puertas a sus ideas, acompañarlos y contenerlos.

Hoy tenemos una gran responsabilidad y los que estamos en política debemos ser los portavoces y allanar el camino para los que vienen a nuestro lado, debemos demostrar como grupo que cualquier joven está preparado para ocupar un lugar, es la oportunidad de seguir sumando para aportar desde donde nos toque estar a la gran gestión que desde hace años se vienen llevando adelante a nivel provincial y en muchas localidades.

Bella Vista es una ciudad que viene creciendo a pasos agigantados, en ese sentido también crecen las políticas públicas en materia de infraestructura como también de salud, asistencia y contención, son los lineamientos básicos para el desarrollo de cada comunidad, el deber de planificar frente a cada grupo social, frente a cada grupo etario es esencial porque cada cual presenta una realidad y una demanda diferente.

Como Estado hemos apostado a diagramar actividades que den lugar a la inclusión del ciudadano en cada acción, necesitamos promover el sentido de pertenencia frente a cada servició o beneficio municipal, de esa manera es posible afianzar los vínculos Estado-Comunidad.

Se han desarrollado programas y políticas integrales, con el aporte de cada dependencia municipal, dirigida a cada población con metas concretas:

**Niñez:** Es indispensable generar un nexo de contención y asesoramiento acerca de los cuidados mínimos en la primera infancia, sabemos que cada niño es un gran proyecto de vida, es necesario brindar herramientas a las familias para frente a las demandas y necesidades puedan encontrar en el Estado una respuesta.

**Adolescentes:** Es una etapa de la vida en la cual nos encontramos frente a un sinfín de cuestionamientos y una dificultad para tomar ciertas decisiones, se han desarrollado talleres de orientación vocacional para los jóvenes de la localidad, talleres de prevención de consumo puesto que reconocemos que son nuestra población más expuesta y vulnerable frente a este flagelo, talleres de educación sexual con un perspectiva de género pero también de derecho en donde la decisión personal es esencial para cada adolescente.

**Adultos:** existen grandes aristas que han requerido la atención del Estado, fundamentalmente se han gestionado talleres de oficio, se han contenido demandas frente a situaciones de vulnerabilidad social, cotidianamente el eje central de las acciones pasa por mejorar los estándares de vida, estar presentes en los momentos difíciles.

**Adultos Mayores:** nuestro objetivo fundamental es generar espacios de participación de recreación de momentos compartidos, nos hemos propuesto poder recobrar el rol e identidad de nuestros adultos mayores y cuán importante son para nuestra sociedad, su sabiduría, su experiencia sus caminos recorridos son ejemplos que las nuevas generaciones debemos escuchar y brindarles las herramientas para que puedan seguir siendo y sintiéndose parte de nuestra vida en comunidad.

La discapacidad es tema que nos moviliza cotidianamente porque sabemos que tenemos que estar a la altura de las necesidades y demandas de las personas con discapacidad, la accesibilidad en materia de infraestructura urbana pero también el desarrollo personal para una vida en sociedad. Una de las acciones más importantes fue poder articular con comercios locales **pasantías laborales**, las cuales nos permitieron aportar esa mirada inclusiva que muchas veces esta en el discurso y no en las acciones.

_Nicolás Scófano, Trabajador Social. Secretario de Desarrollo Humano de la localidad de Bella Vista._