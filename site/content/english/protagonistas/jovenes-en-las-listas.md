+++
author = "Ruben Caserotto"
author2 = ""
date = 2021-07-26T11:00:00Z
description = ""
image = "/images/placa-p10-ruben.jpg"
image_webp = "/images/placa-p10-ruben.jpg"
title = "Jóvenes en las listas - Mariano I. Loza"

+++
Rubén Caserotto, presidente de la Juventud Radical de Mariano I. Loza, es actual candidato a Intendente de su localidad.

¡Celebramos que más jóvenes se involucren y tomen el desafío de ocupar espacios de poder!

Mirá el video completo acá: [4-Kffx4vYeQ](https://youtu.be/4-Kffx4vYeQ "4-Kffx4vYeQ") 