+++
author = "Juventud Radical Formosa"
author2 = ""
date = 2021-02-08T14:00:00Z
description = ""
image = "/images/placa-p5-jr-formosa-2.jpg"
image_webp = "/images/placa-p5-jr-formosa-2.jpg"
title = "Situación de Formosa"

+++
En un video realizado por militantes de la Juventud Radical de Formosa, nos cuentan la situación que viven respecto a la pandemia y el gobierno actual.

Para verlo, ingresá en: [Protagonistas - JR Formosa](https://youtu.be/D9A6EgZg34Q "Protagonistas - JR Formosa")

¡Gracias por ser protagonistas trabajando juntos por el cambio! Adelante Radicales.