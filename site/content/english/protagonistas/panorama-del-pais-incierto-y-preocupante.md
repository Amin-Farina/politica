+++
author = "Martin Tognola"
author2 = ""
date = 2021-04-29T12:00:00Z
description = ""
draft = true
image = "/images/nota-martin-1.jpg"
image_webp = "/images/nota-martin-1.jpg"
title = "Panorama del país incierto y preocupante"

+++

Si buscamos la definición de autocracia en el Diccionario de la Real Academia Española, nos encontraremos con:

“Forma de gobierno en la cual la voluntad de una sola persona es la suprema ley”.

Paulatinamente, y como dijo Marcelo Longobardi, Argentina se encuentra virando desde una democracia compleja a una autocracia, donde el presidente está tomando decisiones inconsultas y sorpresivas, perjudicando enormemente al sector gastronómico e interrumpiendo nuevamente las clases presenciales, con las consecuencias que conlleva (sociales, mentales, físicas y académicas en los estudiantes).

Debemos recordar que el consenso y el diálogo son los pilares fundamentales que otorgan sustento a la calidad democrática de los Estados.

El panorama de nuestro país es cada vez más incierto y preocupante, con:

\- un índice de pobreza que llegó al 42% al término del segundo semestre del 2020 (los nuevos porcentajes informados implican que 19,2 millones son pobres, y entre ellos, 4,5 millones son indigentes)

\- una inflación que es asfixiante y no da tregua, afectando considerablemente el poder adquisitivo y el bolsillo de los argentinos.

\- un humor social caracterizado por la fragilidad e inestabilidad (que es muy entendible, a causa de un hartazgo colectivo producto del confinamiento excesivo, de la falta de planificación del gobierno nacional y de un ritmo de vacunación menor al pautado)

\- el máximo mandatario que sigue demostrando, de manera reiterativa, incoherencias, contradicciones e improvisaciones en sus discursos y toma de decisiones, reflejando una pérdida de su autoridad y legitimidad, y manifestando una actitud arbitraria, violenta (calificó de imbéciles y miserables a dirigentes opositores) y autoritaria frente a las críticas y reclamos de políticas pertinentes que solucionen el fracaso de la gestión de la pandemia.

Y, por si fuera poco, acusó al sistema sanitario de “relajarse”, a los médicos, enfermeros y a todo el personal de salud, que trabajan incansablemente, empujados por el amor a su profesión y a la vida, y por un altruismo y abnegación que merecen el reconocimiento absoluto por parte de la sociedad, ellos son los que están al pie del cañón, nuestros héroes que transmiten esperanza, vestidos de blanco, color que simboliza la paz, la bondad y la pureza.

Por Martin Tognola.