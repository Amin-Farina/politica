+++
author = "Ignacio Enriquez Orzusa"
author2 = ""
date = 2021-03-16T12:00:00Z
description = ""
image = "/images/placa-p6-nacho.jpg"
image_webp = "/images/placa-p6-nacho.jpg"
title = "Es una buena oportunidad"

+++
La educación fue, es y será la herramienta fundamental que la sociedad posee para la transmisión de cultura. Formal o informal, la transferencia de información y costumbres hace al desarrollo humano, es por eso que desde que nos empezamos a denominar argentinos, la educación es una política de Estado.

Desde una mirada platónica en el Estado ideal los gobernantes no legislan, educan, crean hábitos y costumbres, la educación tiene, categoría de la más elevada función pública. En la aparición del ideario pansófico, en el siglo XVII, en la cual la premisa es enseñar todo a todos, dio el origen a la escuela como la conocemos hoy. El Estado argentino en su comienzo, encontró en la educación la solución a grandes problemáticas que lo asechaba, como la unidad nacional.

En el Primer Congreso Pedagógico Nacional que se realizó en el año 1882, se discutió, como sociedad argentina qué tipo de educación se instauraría. Esto dio lugar a la Ley 1420 donde se plasma la enseñanza laica, gratuita y obligatoria, y la aparición del sistema educativo argentino. Tan controvertida al principio, pero consensuado posteriormente.

Nuestra educación nos llevó a unirnos con fines claros para el desarrollo de nuestra nación. Fuimos ejemplo en el mundo al llevar a la población a un porcentaje óptimo de instrucción básica. Pero el traspié apareció en la República Argentina, y la educación no fue ajena a eso.

Una era oscura en la Argentina, lleno de atropellos a los Derechos Humanos, culmina con el advenimiento de la democracia en el año 1983, la crisis nos da una nueva oportunidad, y la educación también se nutre de ello. Durante la presidencia de Raúl Alfonsín se llevó adelante un nuevo congreso pedagógico nacional, acontecimiento importante, donde todos, ciudadanos argentinos, sin distinción alguna, pudieron participar e incidir en ella. Después de situaciones tumultuosas en la educación encontramos una vez más el espacio para buscar la unión, pero aun así la solución a los paradigmas de aquel entonces no encontró soluciones concretas, y los de hoy en día, chocan estrepitosamente ante una bofetada a la humanidad.

Hoy el ser humano lucha con un enemigo invisible, enemigo que atenta con nuestra salud, situación extraordinaria que llevo a tomar decisiones políticas extraordinarias, y la educación no escapa de ello. En nuestro país pasamos un ciclo lectivo completo sin pisar las escuelas, esto llevó a agotar el ingenio de los profesionales de la educación para llegar a estudiantar, gesta que según valoraciones personales tan solo será apreciado. Sacarnos de ese foco tradicionalista que vemos a la escuela, del edificio, del pupitre, del pizarrón enfrente, de la socialización en la asistencia nos puede llevar a un debate de una nueva lógica de educación, donde el Estado, la escuela y la sociedad civil encuentren las herramientas necesarias para una superación nacional, somos la Argentina y debemos apuntar al éxito.

La Unión Cívica Radical desde su concepción filosófica como partido, puso a la educación como eje fundamental, la pluma en su escudo, la simboliza, da prueba de ello, pero no queda allí tan solo los hechos también hablan. Desde la llegada de la UCR al poder impulso las reformas que competen a la educación, la reforma universitaria da ejemplo de ello, la lucha en contra de un régimen instaurado en las universidades busca la apertura en un amplio sentido.

Las luchas no terminaron allí, todo lo contrario, han seguido con el pasar de los años, donde una vez más, acompañado de un gobierno radical, la educación encontró el ámbito para que como sociedad la discutamos para encontrar soluciones. Pero la lucha sigue, y desde nuestra moral radical estamos al servicio de la educación, para que esta desde su función primordial en la sociedad, nos lleve al éxito que como Argentina estamos destinados.

En la educación está la clave, en un alto grado nosotros somos, en nuestras virtudes y defectos, un resultado de las virtudes y defectos de la educación que recibimos. La destreza y aptitudes de la sociedad para vencer los obstáculos y realizar sus propósitos, para adquirir y desenvolver una individualidad en el mundo, para crear, para fundar, para cimentar, esta signada desde su educación.

Por Ignacio Enriquez Orzusa.   
Secretario de Políticas Públicas del Comité Nacional de la Juventud Radical.