+++
author = "Ricardo Cayo Romero"
author2 = ""
date = 2021-08-18T14:00:00Z
description = ""
image = "/images/placa-p13-cayo-romero.jpg"
image_webp = "/images/placa-p13-cayo-romero.jpg"
title = "Jóvenes en las listas - Colonia Pando"

+++
Ricardo "Cayo" Romero, es actual presidente de la Juventud Radical y candidato a Intendente de la localidad de Colonia Pando.

Celebramos los desafíos que toman los jóvenes de nuestro partido centenar. ¡Adelante Radicales!

Mirá el video completo acá: [https://youtu.be/bcRzGiganp8](https://youtu.be/bcRzGiganp8 "https://youtu.be/bcRzGiganp8")