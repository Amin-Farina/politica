+++
author = "Martin Tognola"
author2 = ""
date = 2021-06-24T13:00:00Z
description = ""
image = "/images/placa-p8-martin2.jpg"
image_webp = "/images/placa-p8-martin2.jpg"
title = "¿Qué significa la izquierda y la derecha en el espectro político?"

+++
En la actualidad es frecuente escuchar a jóvenes, dirigentes políticos o grupos sociales decir que pertenecen a la izquierda o a la derecha. 

Pero es necesario preguntarse: ¿comprendemos con exactitud y profundidad el significado de estos dos términos?, ¿podemos posicionar nuestros pensamientos, valores y principios dentro del espectro ideológico?, ¿la vida política se agota con esta polaridad?, ¿existen nuevas formas de pensar dentro del escenario político actual?

Seguramente querido/a lector/a tendrás el mismo interés que yo por responder el interrogante principal: ¿Qué significa la izquierda y la derecha en el espectro político?

La respuesta la construiremos juntos, ustedes mismos podrán sacar sus propias conclusiones. Espero poder ayudarlos a clarificar y comprender sobre esta cuestión.

Para comenzar, sabemos que las discusiones políticas y los debates siempre están girando sobre esta dicotomía (izquierda-derecha). Podemos decir que son términos considerados opuestos, que se emplean para designar una contraposición y un contraste entre las ideologías, intereses, valoraciones y acciones políticas.

Tienen su origen conceptual durante la Revolución Francesa, más precisamente, en la Asamblea Nacional Constituyente, es decir, hace más de dos siglos. En síntesis, en esta Asamblea, los jacobinos que formaban parte del grupo radicalizado y revolucionario se sentaban en el ala izquierda y los girondinos en el ala derecha, que eran partidarios de posturas conservadoras.

Una manera de ejemplificar de manera clara y precisa el tema que nos compete, es utilizar la siguiente metáfora de Bobbio (1996), reconocido jurista, filósofo y politólogo italiano.

_“Los que miran al sol del porvenir de los que actúan guiados por la inagotable luz que viene del pasado”_. En esta frase, distingue a los innovadores de los conservadores, a los progresistas de los tradicionalistas. En el lenguaje político se considera que la conservación es de derecha, dirigida a mantener la jerarquía social, la tradición, la historia y la defensa del pasado como rasgo característico; y, por otro lado, el cambio y el progreso pertenecen a la izquierda, dirigida principalmente a la emancipación, innovación e igualdad.

Para indagar y profundizar sobre el argumento central, se puede tener en cuenta distintos principios y criterios. Uno de ellos fue el **principio político de conservadorismo-progreso**, que lo mencionamos anteriormente.

Ahora avancemos con la **díada igualdad- desigualdad**. Es el criterio más utilizado para distinguir la derecha de la izquierda. Esto nos debe hacer reflexionar: ¿Qué actitud asumo yo como ciudadano frente al ideal de la igualdad? ¿estoy dispuesto a luchar cotidianamente por este ideal? ¿Qué percepción y valoración tengo frente al concepto de igualdad? Podemos decir que la izquierda parte de la convicción de que las desigualdades son sociales, y, por lo tanto, eliminables o en menor medida, posibles de aminorar o reducir los factores que las producen; en cambio, para la derecha, las desigualdades son consideradas naturales e ineliminables.

Otro binomio que permite diversificar la clarificación entre derecha e izquierda es la **relación entre la libertad y el orden y la autoridad.**

Si en este momento un educador o educadora, o cualquier persona que le apasione el ámbito de la educación, está leyendo este artículo, seguramente le resultará familiar esta pregunta ¿Cómo enfrentar el reto de educar a los jóvenes para que no se conviertan en pequeños tiranos ni en seres inhibidos? Para Freire, existe una tensión dialéctica en las relaciones entre autoridad y libertad.

Entonces, volviendo a la cuestión política, sin dudas que esta delicada y compleja tensión constantemente se está manifestando y produciendo en el escenario político. Para evitar el límite extremo del Estado totalitario o el de la anarquía, donde ambas niegan la democracia y las libertades individuales, es necesario un compromiso y un equilibrio mutuo de ambas partes (de la libertad y de la autoridad).

Cabe recordar que en la actualidad existen libertarios y autoritarios en ambos campos de la delimitación política y que un extremista de izquierda y uno de derecha tienen en común como afiliación política, la antidemocracia. Los extremos se tocan, como se dice cotidianamente.

Desde hace siglos se acepta que el hombre es un ser social: la persona no puede darse en singular. Al analizar la relación entre la persona y la sociedad encontramos dos posturas: pensar que la persona debe estar sometida al conjunto social, o, por el contrario, dar la preponderancia a las personas. La primera postura es de izquierda, el hombre determinado por el medio social. La segunda postura corresponde a la derecha, que piensa más bien en un individuo gestor de su propia vida, ejerciendo la libertad y sus responsabilidades personales. Para finalizar este punto, el valor político supremo de la izquierda es la igualdad, y para la derecha, la libertad es el valor superior.

En cuanto a la economía, a grandes rasgos, la izquierda considera primordial la regulación y planificación estatal. La derecha confía más en el mercado y la iniciativa privada.

Raúl Alfonsín, en una entrevista que le realizaron, sostuvo la necesidad e importancia del rol del Estado en la economía y en el mercado: “…el mercado es importante, pero tiene que haber un rol del Estado, un rol del Estado en el mercado, un rol regulador, primero que nada, usted sabe que, si no, aparece el monopolio[\[1\]](#_ftn1), el oligopolio[\[2\]](#_ftn2)…”

Para concluir, teniendo en cuenta nuevamente a Bobbio (1996) podemos agrupar las doctrinas y movimientos políticos en cuatro grandes corrientes:

\- Las de extrema izquierda: representan a los movimientos y teorías portadoras de fuertes concepciones igualitaristas, pero a la vez, visiones autoritarias y excluyentes del poder.

\- Las de centro izquierda: doctrinas y movimientos libertarios y a la vez igualitarios. Por ejemplo, los partidos socialdemócratas

\- Las de centro derecha, combinan posiciones libertarias con doctrinas que fundamentan las desigualdades. Tienen una visión puramente jurídica tanto de la igualdad como de las libertades.

\- Las de extrema derecha, que son a la vez antiigualitarias y anti libertarias, utilizan a las diversas formas de autoritarismo como la esencia de su concepción de poder.

**Conclusión**

Debemos considerar al binomio izquierda y derecha como un punto de partida de una más amplia definición del mapa político actual. Los dos términos se usan para designar una actitud política, pero no son constantes, están en permanente construcción y evolución, y representan más bien una mentalidad.

Hoy en día, se habla de una crisis de las ideologías, de la desafección de política, de la disolución de los pensamientos ideológicos e inclusive, algunos autores como Fukuyama hacen referencia al “punto final de la evolución ideológica de la humanidad”. Es verdad que últimamente podemos sentir que nuestra identidad política forjada por los valores, creencias y principios que tenemos y que son construidos mediante la trayectoria de vida que cada persona realiza, no la podemos reflejar en algún representante político o espacio partidario, es decir, que no podemos brindarle un sentido de pertenencia.

Lo que es ineludible dejar en claro, es que la vida política no se agota con esta polaridad. En el escenario político actual se incorporan nuevas voces, nuevas maneras de pensar, de sentir y de actuar, y cobran protagonismo problemáticas transversales que tienen un sentido más complejo y profundo que esta dicotomía de izquierda-derecha. Por ejemplo, en el siglo XX surgieron nuevos movimientos sociales: feminismo, pacifismo y ecologismo, que se están moviendo en un escenario parcialmente distinto y reclaman nuevas formas de expresión política.

Considero que lo más significativo es seguir creyendo y sosteniendo que, a pesar de nuestras inclinaciones políticas o diferencias ideológicas: el consenso, el debate, el diálogo, el respeto a las instituciones, la honradez republicana y la defensa de la República, son valores esenciales en la política y deben estar siempre vigentes.

Entre todos los actores podemos lograr un país más justo, democrático y equitativo, y mejorar de forma sustancial y de manera auténtica, los distintos aspectos y ámbitos de la sociedad argentina. Es un trabajo en conjunto y requiere mayor compromiso social y ético y participación ciudadana.

Mientras sigamos siendo ciudadanos de este maravilloso país, debemos seguir apostando por un futuro mejor para todos.

Por último, les comparto este sencillo test político, [https://www.testpolitico.com/](https://www.testpolitico.com/ "https://www.testpolitico.com/") que es una herramienta útil que permite la orientación y la ubicación de nuestra ideología dentro del espectro político, teniendo en cuenta dos dimensiones: social y económica.

Martin Tognola.-

**Bibliografía**:

\- BOBBIO, N. (1996) “_Derecha e Izquierda. Razones y significado de una distinción política”_

\- [https://www.youtube.com/watch?v=ZnivxhROd6Y](https://www.youtube.com/watch?v=ZnivxhROd6Y "https://www.youtube.com/watch?v=ZnivxhROd6Y") Homenaje a Raúl Alfonsín

\- NAVAS, A. (2014) _Izquierda y derecha: ¿Una tipología válida para un mundo globalizado?_

**Referencias**:

***

[\[1\] ](#_ftnref1)El monopolio es una estructura de mercado en donde existe un único oferente de un cierto bien o servicio, es decir, una sola empresa domina todo el mercado de oferta

[\[2\] ](#_ftnref2)Un oligopolio es una estructura de [mercado](https://economipedia.com/definiciones/mercado.html) en donde existen pocos competidores relevantes y cada uno de ellos tiene cierta capacidad de influir en el precio y cantidad de equilibrio.