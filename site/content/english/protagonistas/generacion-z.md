---
title: La Generación Z, a las urnas
date: 2020-10-02T20:00:00.000+00:00
author: Ignacio Maldonado Yonna
author2: Francisco Bosco
image_webp: "/images/placa-p.jpg"
image: "/images/placa-p.jpg"
description: El 31 de octubre del 2012 se sancionó la ley 26774 en la cual se extendió
  el derecho a votar en elecciones nacionales a los jóvenes de 16 y 17 años.

---
El 31 de octubre del 2012 se sancionó la ley 26774 en la cual se extendió el derecho a votar en elecciones nacionales a los jóvenes de 16 y 17 años.

Con la sanción de la ley nacional, la mayoría de las provincias comenzaron a adaptar su legislación para permitir el voto joven en las elecciones provinciales. En el mismo año 2012 se sumaron 9 provincias, en 2013 adhirieron 6, y entre 2014 y 2017 otras 5. Santa Cruz y Salta lo permiten de hecho, al utilizar para las elecciones provinciales el padrón nacional. A la fecha, el voto joven sólo no está previsto en Corrientes y Santa Fe. Su primera implementación fue en las elecciones legislativas nacionales del 2013. El padrón de jóvenes aumentó casi en un 50% desde su implementación hasta las elecciones generales del 2017, pasando de 627.364 electores a 1.129.824 (a nivel nacional).

![](/images/novedades/imagen-dentro-de-post.jpg)

Fuente: Página oficial del Ministerio del Interior, Gobierno Nacional.

El último avance en cuanto al proyecto provincial de “Voto Joven” fue en noviembre de 2018 cuando el gobernador remitió el proyecto a la legislatura provincial que no tuvo mucho avance.

En las elecciones nacionales del año pasado (2019) hubo 10013 jóvenes entre 16 y 17 habilitados para votar, de los cuales el 30% corresponden a la jurisdicción Capital.

A nivel provincial el voto joven representa el 1.14 % de los votos en toda la provincia. El municipio con menor porcentaje de votantes jóvenes es Estación Torrent, con un total de 0.25% del electorado y el de mayor porcentaje de votantes es el municipio de la Cruz con 2% de su electorado. Encontrándose arriba en un 0.86% del promedio provincial.

#### LA GENERACIÓN Z

De aprobarse el proyecto de voto joven toda la Generación Z, en su totalidad, podría concurrir a las urnas el próximo año. La generación Z (nacidos entre 1994 y 2010) son los primeros chicos nativos digitales y eso los ha formado como independientes, autodidactas y mucho más preparados a las formas de empleo del futuro.

La consultora Ipsos publicó el año pasado un informe sobre la Generación Z y su vinculación con la política. En ese informe se los define como la “generación del otro”.

La generación Z no creen en las soluciones individuales cuando los problemas son colectivos. Se ven identificados con las causas no con las instituciones, son plurales, construyen desde la micropolítica por ser imprevisible, por moverse entre los bordes, siempre y en cualquier aspecto de la vida.

Pensar y actuar desde una concepción de lo micro político implica entender que el cambio puede producirse desde lo espontáneo, desde cualquier rincón de la realidad, acción y actividad.

Algunas de las causas que los convocan son la igualdad de género, respeto por la diversidad, la equidad social y la protección del medio ambiente, entre otras. Se identifican con causas que le da importancia a valores universales.
Analizando el padrón vemos que este grupo etario entre 16 y 25 representa el 19.7% del padrón, dando un total de 172.650 votantes en toda la provincia. El municipio con menor porcentaje de votantes de generación Z es Riachuelo, con un total de 11,7% del electorado y el de mayor porcentaje de votantes es el municipio de la San Miguel con 26.7% de su electorado. Encontrándose arriba en un 7% del promedio provincial.

Será un desafío para los partidos políticos ver cómo llamar la atención de esta generación y hacerlos partes de la construcción del futuro.

_Nota de Opinión para el Diario El Litoral._

**Ignacio Maldonado Yonna.**

**Francisco Bosco.**