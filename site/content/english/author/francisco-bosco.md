---
title: "Francisco Bosco"
image: "/images/autores/francisco-bosco.jpg"
email: ""
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "https://facebook.com/boscojfrancisco"
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/boscojfrancisco"
  - icon : "ti-instagram" # themify icon pack : https://themify.me/themify-icons
    link : "https://instagram.com/boscojfrancisco"
---

Lic. en Sistemas de Información, docente de FaCENA.
Director de Estadística y Censos de la Provincia de Corrientes.