+++
email = ""
image = "/images/stella.JPG"
social = []
title = "Stella Maris Folguerá"

+++
Stella Maris Folguerá es artista plástica, escritora, fue la primer candidata mujer a Intendente en el año 1985 cuando no había cupo, quedando como concejal hasta el 1989 y fue presidenta de la Convención Provincial del partido entre los años 1990-1992.