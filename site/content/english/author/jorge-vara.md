+++
email = ""
image = "/images/photo4958711924632168652.jpg"
title = "Jorge Vara"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/jorgevaraok/"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/JorgeVara8"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/jorge.vara.75098"

+++
Ingeniero agrónomo. Diputado de la Nación por la Provincia de Corrientes, electo por ECO+Juntos por el cambio. Ex Ministro de Producción de la provincia de Corrientes.