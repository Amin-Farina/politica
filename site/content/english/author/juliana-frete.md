+++
email = ""
image = "/images/photo4958711924632168651.jpg"
title = "Juliana Frete"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/julianafrete/"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/JulianaFrete"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/yuyooo"

+++
Militante de la Juventud Radical y parte del Comité de la Provincia de Corrientes.

Feminista. Estudiante.