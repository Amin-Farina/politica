+++
email = ""
image = "/images/photo4906677313509370094.jpg"
title = "Lautaro Barbis"
[[social]]
icon = "ti-facebook-alt"
link = "https://www.facebook.com/lautaro.barbis"
[[social]]
icon = "ti-twitter"
link = "https://twitter.com/lautarobarbis"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/lautarobarbis/"

+++
_Paso de la Patria.   
Estudiante de Ciencia Política. Militante de la Unión Cívica Radical._