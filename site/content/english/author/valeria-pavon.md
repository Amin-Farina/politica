+++
email = ""
image = "/images/photo_5168182556309433011_x.jpg"
title = "Valeria Pavon"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/valeria.pavon.125"
[[social]]
icon = "ti-twitter-alt"
link = "twitter.com/valeriapavonok"
[[social]]
icon = "ti-instagram"
link = "instagram.com/valeriapavonok"

+++
Presidenta de la Juventud Radical de la Argentina.

Ex Directora de Juventud de la Provincia de Corrientes y candidata a Diputada Provincial.