+++
email = ""
image = "/images/photo4958711924632168650.jpg"
title = "Nicolas Scofano"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/NiCuChO808"
[[social]]
icon = "ti-instagram"
link = "www.instagram.com/nicucho_scofano"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/nicolas.scofanobadanlopez"

+++
Presidente de la Convención de la Juventud Radical de la Provincia de Corrientes.

_Trabajador Social. Secretario de Desarrollo Humano de la localidad de Bella Vista._