+++
email = ""
image = ""
title = "Ignacio Enriquez Orzusa"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/ignacioenriquezorzusa/"

+++
Futuro profesor en Ciencias Políticas  
Militante Radical  
Secretario de Políticas Publicas del Comité Nacional de la Juventud Radical.