+++
email = ""
image = "/images/photo5055500661914970327.jpg"
title = "Valentin Alcaraz"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/DanielValentin1"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/alcarazpradorojo"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/alcarazvalentinok/"

+++
Coordinador Nacional UCR Diversidad.

Militante radical. Presidente del comité de Juventud Radical de San Luis del Palmar.