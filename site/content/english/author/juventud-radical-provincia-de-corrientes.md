+++
email = ""
image = "/images/logo-jr-provincia.jpg"
title = "Juventud Radical Provincia de Corrientes"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/jrcentral0"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/jrcentralctes"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/juventudradicalcorrientes/"

+++
Comité de la Provincia de Corrientes 