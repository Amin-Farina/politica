+++
email = ""
image = "/images/gustavo-valdes.jpg"
title = "Gustavo Adolfo Valdés"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/GustavoValdesOk/"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/gustavovaldesok"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/gustavovaldesok/"

+++
Gobernador de la Provincia de Corrientes.

Político. Unión Cívica Radical.