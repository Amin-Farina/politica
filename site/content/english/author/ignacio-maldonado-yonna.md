---
title: Ignacio Maldonado Yonna
image: "/images/autores/ignacio-yonna.jpg"
email: ''
social:
- icon: ti-facebook
  link: https://facebook.com/ignaciomalyon
- icon: ti-twitter-alt
  link: https://twitter.com/ignaciomy
- icon: ti-instagram
  link: https://instagram.com/ignaciomy

---
Abogado, especializado en Gestión y Control de Políticas Públicas; master en Planificación del Desarrollo Local y Regional. 

Secretario de Ambiente en Municipalidad de la Ciudad de Corrientes.