+++
email = ""
image = "/images/photo5168114210494851262.jpg"
title = "Carolina Caamaño"
[[social]]
icon = "ti-instagram"
link = "instagram.com/tataicha"
[[social]]
icon = "ti-twitter-alt"
link = "twitter.com/tataicha"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/CARITO.CORRIENTES"

+++
Periodista. Feminista. Militante Unión Cívica Radical.

Directora de Género de la Ciudad de Corrientes.