+++
email = ""
image = "/images/photo4952046865533216941.jpg"
title = "Emilio Cornaglia"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/EmilioBuhoCornaglia"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/buhocornaglia"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/buhocornaglia/"

+++
Abogado UNC. Radical y Reformista. Ex Presidente de la FUA y Ex Sec. Gral de Franja Morada Nacional.