+++
email = ""
image = "/images/photo_4917971561374395196_y.jpg"
title = "Tiago Puente"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/TiagoIgnacioPuente"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/TiagoPuente"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/tiagopuente/"

+++
Diputado Provincial de Catamarca.

Secretario General de la Juventud Radical Nacional.