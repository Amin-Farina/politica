+++
email = ""
image = "/images/photo5168114210494851275.jpg"
title = "Noel Eugenio Breard"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/NoelBreardCtes"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/noelbreard"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/noelbreard/"

+++
Abogado. Senador Provincial de Corrientes. Especialista en Derecho Administrativo. Ex docente de Derecho Civil.