+++
email = ""
image = "/images/photo_5143300367654694074_y.jpg"
title = "Santiago Merino"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/profile.php?id=100074826846492"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/SantiM94"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/santiagomerinook/"

+++
_Contador Público._ Docente en FCE-UNNE.

_Gerente General Fideicomiso de Administración Santa Catalina._

_Presidente Comité Capital de la Juventud Radical._

Coordinador General Jóvenes CPCE.