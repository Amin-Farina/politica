+++
email = ""
image = "/images/photo4952046865533216940.jpg"
title = "Florencia Ojeda"
[[social]]
icon = "ti-facebook"
link = "https://www.facebook.com/florojedaconcejal"
[[social]]
icon = "ti-twitter-alt"
link = "https://twitter.com/florojedaok"
[[social]]
icon = "ti-instagram"
link = "https://www.instagram.com/florojedaok/"

+++
Abogada. Radical. Concejal de la Ciudad de Corrientes.

Vicepresidente del Comité Capital de la Unión Cívica Radical Corrientes.