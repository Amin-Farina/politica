---
title: "Luciana Tassano"
image: "https://gitlab.com/Amin-Farina/imagenes/-/raw/master/Politica/luicana-tassano.jpg"
email: ""
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/luchi.tassano"
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.twitter.com/LucianaTassano"
  - icon : "ti-instagram" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.intagram.com/Lucianatassano"
---

Abogada. Mg. Derecho Tributario